# destinieTutorial

Ce projet a pour objectif de constituer une documentation interactive du modèle de microsimulation des retraites Destinie 2.
* __Lien du site internet :__ http://destinie.gitlab-pages.insee.fr/destinietutorial
* __Niveau de détail de la documentation :__ à mi-chemin entre la documentation exhaustive du code source et les études publiques qui traitent du modèle Destinie.
* __Cible :__ nouveaux-venus aux postes RPS Destinie, collaborateurs fréquents (DG Trésor, COR), chargés d'études souhaitant utiliser le modèle pour des questions de recherche.
* __Format :__ livre interactif (Rmarkdown / Bookdown)

Les chapitres proposés sont les suivants :

0. [__Introduction__](http://destinie.gitlab-pages.insee.fr/destinietutorial/index.html) : récapitule les objectifs du projet;
1. [__Préparatifs pour réaliser une simulation__](http://destinie.gitlab-pages.insee.fr/destinietutorial/chap1.html);
2. [__Structures principales du modèle__](http://destinie.gitlab-pages.insee.fr/destinietutorial/chap2.html) : la logique générale du programme C++ et les structures principales à assimiler;
3. [__Module démographie__](http://destinie.gitlab-pages.insee.fr/destinietutorial/cha3.html);
4. [__Module des transitions professionnelles__](http://destinie.gitlab-pages.insee.fr/destinietutorial/chap4.html);
5. [__Module des retraites__](http://destinie.gitlab-pages.insee.fr/destinietutorial/chap5.html);
6. [__Réaliser simplement une simulation__](http://destinie.gitlab-pages.insee.fr/destinietutorial/chap6.html);
7. [__Variantes de simulation__](http://destinie.gitlab-pages.insee.fr/destinietutorial/chap7.html) : réalisation de variantes simples (ex: changement de cibles économiques) mais aussi plus complexes impliquant la modification du code source (ex : implémentation d'une réforme des retraites).

Les prochains chapitres envisageables sont : 

- Création des fichiers d'hypothèses démographiques et économiques
- (Modifications de l'enquête Patrimoine pour créer les données source de Destinie)
