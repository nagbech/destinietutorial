




# Préparatifs pour réaliser une simulation {#chap1}

Ce chapitre détaille les différents éléments qui sont nécessaires pour réaliser des simulations avec le modèle Destinie 2.

## Programme: Une utilisation depuis R

Pour améliorer sa rapidité d'éxécution, le coeur du code source de Destinie 2 a été développé en _C++_. Toutefois, l'utilisation du modèle se fait directement dans le logiciel _R_, plus simple d'utilisation et davantage utilisé dans la statistique publique. L'utilisateur importe les éléments requis pour la simulation (données source, options de simulation) et lance les fonctions de simulation de Destinie depuis R. Ces fonctions se chargent d'importer l'ensemble des éléments dans des objets compris par _C++_, de réaliser les différentes opérations de microsimulation, et d'exporter dans R les outputs en sortie de simulation. Ce jonglage, invisible pour l'utilisateur, est permis par la librairie _Rcpp_. Notons également que l'utilisateur ne doit pas se soucier des problématiques liées au compilateur _C++_: RStudio dispose nativement d'un compilateur _C++_, et aucune action spécifique est requise sur ce plan là pour que Destinie 2 fonctionne correctement.

Destinie 2 est disponible sous la forme d'une librairie R classique qui est disponible via un [dépôt github public ](https://github.com/InseeFr/Destinie-2). Depuis sa mise en ligne, celui-ci n'a pas été entretenu. Nous planifions de mettre à jour ce dépôt chaque année, en important les modifications majeures (mise à jour de la législation, debug) que nous ajoutons ponctuellement à la version "locale" de Destinie sur laquelle l'équipe travaille au quotidien pour réaliser ses simulations. <span style="color:blue"> Si le lecteur est amené à travailler sur la version locale de Destinie, nous l'invitons à consulter le document ... pour qu'il puisse se greffer au dépôt local de Destinie.</span> 

En plus de l'import de la librairie Destinie depuis le dépôt github public, d'autres librairies complémentaires doivent être téléchargées par l'utilisateur pour lancer ses premières simulations:


```r
# devtools (nécessaire pour que la librairie Destinie fonctionne)
library(devtools)
# pour s'assurer que devtools est correctement installé, la fonction ci-dessous doit afficher True
find_rtools()
```

```
## [1] TRUE
```

```r
# Rcpp (nécessaire pour que la liaison entre R et C++ fonctionne)
library(Rcpp)

# Import de la librairie Destinie
libDestinie = "U:/Destinie/destinie/" #localisation du package destinie
setwd(libDestinie)
devtools::load_all(libDestinie)  # import de la librairie


# librairies "secondaires", utilisées ponctuellement dans un script R standard
library(openxlsx)
library(xlsx)
library(dplyr)
library(tidyr)
library(reshape2)
library(reshape)
library(ggplot2)
require(haven)
library(pracma)
library(readr)
library(readxl)
```

## Données requises en amont des simulations

Pour produire des simulations avec Destinie 2, l'utilisateur doit importer dans sa session R l'ensemble des données nécessaires au modèle. Ces données seront stockées dans R au sein d'un environnement de simulation appelé _simul_. On peut distinguer trois types d'éléments: 
1. Les données utilisées comme source qui seront par la suite projetées par le modèle;
2. Les hypothèses économiques et démographiques sur lesquelles se base Destinie pour réaliser ses projections;
3. Les options de simulation (dernière législation à considérer, pas de simulation, année finale de projection...)

### Données source issues de Patrimoine 2010

La version actuelle du modèle Destinie 2 s’appuie sur l’enquête Patrimoine 2010 en entrée de son générateur de biographies démographiques et professionnelles. Ces données d'enquête sont calées, corrigées et restructurées sous la forme de trois dataframes _ech_, _emp_, _fam_. L'échantillon de départ ainsi constitué comprend 62 633 individus. Le détail de la création des trois données sources depuis l'enquête Patrimoine 2010 n'est pas l'objet de cette documentation. Une explication assez approfondie est donnée dans [le document de travail (M. Bachelet, A. Leduc, A. Marino, 2014)](https://www.insee.fr/fr/statistiques/1381043). Dans AUS, le dossier `V:/DG75-G210/DestinieRetraites_packageR/Genebios/TableSAS/` contient ces trois bases de données.

- _ech_: Cette table contient les caractéristiques des individus (identifiant, diplôme, age de fin d’études, génération, sexe). Elle comprend 62 663 lignes correspondant à tous les individus présents au départ de la simulation, à l’année 2009. Une fois que le générateur des biographiqes démographiques de Destinie aura été lancé, de nouvelles lignes feront leur apparition à chaque naissance simulée.


- _emp_: Elle donne par année la liste des statuts sur le marché du travail, ainsi que le salaire (qui vaut 0 pour l'instant, Patrimoine 2010 ne renseignant pas sur cette donnée). _emp_ se structure sous la forme d'un panel avec 1 ligne par individu et par âge, le dernier âge étant pour le moment celui observé en 2009, Ainsi, dans cette table, un individu qui a 40 ans en 2009 aura 41 lignes pour les âges 0 à 40. Des lignes supplémentaires seront créées lorsque Destinie projetera le futur des trajectoires individuelles de 2010 à l'année de fin de simulation.

- _fam_: La table _fam_ recense les liens familiaux pour chaque individu présent en 2009. Concrètement, cette base donne l’identifiant de chaque personne de sa famille. Cette base à 62 633 lignes, mais une fois les simulations effectuées, elle se structurera sous la forme d’un panel individu * année.


```r
# repertoire où sont stockées les données ech, emp et fam, sous format SAS mais aussi sous format R
rep_rebasage <- "V:\\DG75-G210\\DestinieRetraites_packageR\\Genebios\\TableSAS\\"

# load de simul_patrimoine.Rdata, qui contient ech, emp et fam nettoyées et "prêtes à l'emploi"
load(paste0(rep_rebasage,"simul_patrimoine.Rdata")) 
# -> l'environnement simul contient à ce stade uniquement ces 3 tables
```


```r
head(simul$ech,3)
```

```
##   sexe Id findet typeFP anaiss neFrance k taux_prim moisnaiss emigrant tresdip
## 1    1  1     22      2   1976        1 0         0         1        0       0
## 2    2  2     24      2   1969        1 0         0         2        0       1
## 3    2  3     23      2   2001        1 0         0         3        0       0
##   peudip dipl
## 1      0    5
## 2      0    5
## 3      0    5
```

```r
head(simul$emp,3)
```

```
## # A tibble: 3 x 4
##     age statut    Id salaire
##   <int>  <int> <int>   <dbl>
## 1     0     63     1       0
## 2     1     63     1       0
## 3     2     63     1       0
```

```r
head(simul$fam,3)
```

```
## # A tibble: 3 x 12
##    pere  mere  enf1  enf2  enf3  enf4  enf5  enf6 matri    Id annee conjoint
##   <int> <int> <int> <int> <int> <int> <int> <int> <int> <int> <int>    <int>
## 1 46746 46747     3     4     0     0     0     0     2     1  2009        2
## 2  5296 49755     3     4     0     0     0     0     2     2  2009        1
## 3     1     2     0     0     0     0     0     0     1     3  2009        0
```

#### Obtention des données source

Ce jeu de données n'est pas librement disponible pour toutes les personnes souhaitant réaliser des simulations avec Destinie. Pour les chercheurs et étudiants, il est possible de faire une demande d'accès à une version de ces données via le [réseau Quetelet](http://quetelet.progedo.fr/). Un jeu de données d'illustration est également disponible dans la librairie Destinie, et peut être importé dans l'environnement _R_ en écrivant simplement `data("test")`.

Reconstituer _ech_, _emp_ et _fam_ depuis les données sources de l'enquête Patrimoine 2010 n'est pas chose aisée. Un travail pour améliorer la transparence de cette procédure dite de "rebasage" de Patrimoine a été lancé, et permettra peut-être dans le futur d'intégrer cette étape dans le dépôt public du code source. Nous recommandons pour l'instant aux personnes qui souhaitent produire des simulations avec Destinie de faire la demande d'accès à ces trois bases par le réseau Quetelet.


### Hypothèses démographiques et économiques

Destinie va projeter à partir de 2010 les trajectoires démographiques et professionnelles de l'ensemble des individus initialement présent dans notre bases _simul$ech_. Ces projections se font en cohérence avec des hypothèses démographiques et économiques qui sont mentionnées par l'utilisateur du modèle. Avant de réaliser des simulations, il est ainsi nécessaire d'ajouter à l'environnement _simul_ des données relatives aux hypothèses de projection démographiques et économiques à considérer.

On spécifie en premier lieu le champ (France entière "FE" ou France métropolitaine ("FM")) et l'année finale de simulation (en général 2070):

```r
champ<-"FE" # ou  FM
fin_simul<-2070
```

#### Hypothèses démographiques

Les hypothèses démographiques de projection portent sur les cibles annuelles de fécondité, de mortalité et de migration que Destinie va retenir lors de ses projections. Les cibles démographiques utilisées par Destinie pour des simulations standard proviennent des projections de population actives de l'Insee. Pour ces trois grandeurs, le scénario démographique *central* de l'Insee mais aussi le scénario *haut* le scénario *bas* peuvent simplement être importés en tant que cible démographique dans Destinie.

Les cibles démographiques principales sont stockées sous la forme d'un fichier excel dans le dossier `parametres/Param_demo/ciblesDemographieFE(FM).xls`. Ce fichier centralise les informations provenant de plusieurs fichiers stockés dans le dossier `parametres/Param_demo/`. Ces fichiers contiennent pour chaque scénario les séries de cibles de fécondité,  mortalité et migration.

Dans _R_, on spécifie le type de scénario à considérer dans un dataframe, que l'on exporte dans `parametres/Param_demo/sc_demo.xls`. Le fichier `ciblesDemographie` met ensuite à jour ses données selon la nature du scénario inscrit dans `sc_demo`.


```r
#choix du scenario demographique pour la fécondité, la migration et la mortalité
################
Cent=c("*","","","")
Bas=c("","*","","")
Haut=c("","","*","")
Travail=c("","","","*")
sc_demo=data.frame(fecondite=Cent,esp_vie=Cent,migration=Cent) # choix du scénario démographique

row.names(sc_demo)<-c("Cent","Bas","Haut","Travail")
sc_demo=t(sc_demo)

# écriture du fichier sc_demo.xls qui spécifie le scénario choisi plus haut
wb=xlsx::loadWorkbook(paste0(libDestinie,"parametres/Param_demo/sc_demo.xls"))
feuilles=xlsx::getSheets(wb)
feuille=feuilles[[2]]
xlsx::addDataFrame(sc_demo,sheet = feuille,row.names = T,col.names=T, startRow = 1,startColumn = 1)


xlsx::saveWorkbook(wb, paste0(libDestinie,"parametres/Param_demo/sc_demo.xls")) 

# On ouvre le fichier ciblesDemographie, pour que celui-ci mette à jour ses feuilles selon sc_demo
#openXL(paste0(libDestinie,"parametres/Param_demo/ciblesDemographie_",champ,".xls"))
```

Une fois que `ciblesDemographie` donne bien l'ensemble cibles démographiques qui correspondent au scénario que nous souhaitons considérer, on peut remplir l'environnement `simul` par des objets stockant individuellement les cibles.



```r
## cibles de mortalité 
# ouverture de ciblesDemographie
morta <- xlsx::loadWorkbook(paste0(libDestinie,"parametres/Param_demo/ciblesDemographie_",champ,".xls"))
# Survie (H/F):
simul$Survie_H <- as.matrix(xlsx::readColumns(morta$getSheet("SH"),2,122,2,colClasses = "numeric"))
simul$Survie_F <- as.matrix(xlsx::readColumns(morta$getSheet("SF"),2,122,2,colClasses = "numeric"))
# Quotients mortalité (H/F):
simul$Qmort_H <- as.matrix(xlsx::readColumns(morta$getSheet("QH"),2,122,2,colClasses = "numeric"))
simul$Qmort_F <- as.matrix(xlsx::readColumns(morta$getSheet("QF"),2,122,2,colClasses = "numeric"))
# Espérance de vie (H/F):
simul$espvie_H<- as.matrix(xlsx::readColumns(morta$getSheet("espvieH"),2,122,2,colClasses = "numeric"))
simul$espvie_F<- as.matrix(xlsx::readColumns(morta$getSheet("espvieF"),2,122,2,colClasses = "numeric"))

## cibles de fécondité et migration 

NA0<-function (x)
{return (ifelse(is.na(x),0,x))}

# CiblesDemo: data.frame avec info sur les cibles de naissance et de migration
simul$CiblesDemo <- xlsx::read.xlsx(paste0(libDestinie,"parametres/Param_demo/ciblesDemographie_",champ,".xls"),
                                    sheetName ="CiblesDemo",startRow = 2,colClasses=rep("numeric",48)
                                    )%>%
  mutate_all(funs(NA0))
```

D'autres cibles démographiques doivent être ajoutées à l'environnement _simul_. Ces cibles ne dépendent d'aucun scénario et auront toujours la même valeur. Ce sont les cibles de mortalité différentiée par diplôme et les équations de transition d'état de santé.


```r
# mortalité différentiée par diplôme
simul$mortadiff_dip_F <- xlsx::read.xlsx(paste0(libDestinie,"parametres/Param_demo/MORTA_DIP.xls"),sheetName ="morta_dif_F",startRow = 1)
simul$mortadiff_dip_H <- xlsx::read.xlsx(paste0(libDestinie,"parametres/Param_demo/MORTA_DIP.xls"),sheetName ="morta_dif_H",startRow = 1)
simul$mortalite_diff <-  xlsx::read.xlsx(paste0(libDestinie,"parametres/PARAM_Mortalite_diff.xls"),sheetName = "Mortalite_diff",startRow = 2)

# équations de santé
simul$EqSante <-  xlsx::read.xlsx(paste0(libDestinie,"parametres/sante.xls"),sheetName="adl")
```


#### Hypothèses économiques

Les hypothèses économiques de projection portent sur les cibles d'évolution d'indicateurs macroéconomiques comme le PIB, le chômage ou le salaire moyen par tête (SMPT). Le Conseil d'Orientation des Retraites (COR) fournit ces hypothèses économiques déclinés en plusieurs scénarios. Ces scénarios diffèrent par la valeur de long-terme du taux de chômage et du taux de croissance de la productivité du travail.

Deux fichiers placés dans le dossier `parametres/Projection_COR_2019/` stockent ces hypothèses : `ParamSociaux_2019.xls` et `PARAM_transitions.xls`. Le second fichier donne des cibles annuelles d'inactivité et de chômage par sexe et tranche d'âge. Le premier fichier contient les valeurs cibles annuelles d'évolution du PIB, du SMPT ou encore de l'indice des prix. Notons également que le fichier  `ParamSociaux_2019.xls` contient l'évolution annuelle de paramètres généraux du système de retraite français qui sont utilisés par Destinie lors de son module de simulation des départs à la retraite.

Dans l'environnement simul, deux objets vont être créés afin de stocker le jeu d'hypothèses économiques à considerer pour les simulations: `simul$macro` et `simul$CiblesTrans`


```r
#chargement des paramètres eco

# on détermine le scénario économique à considérer en choisissant un couple de cibles de chômage/productivité de long-terme:
scencho <- "7%" # ou 4,5 ou 10
scenprod <- "1,3%" #ou 1,0 ou 1,5 ou 1,8

# import des fichiers de cibles éco fournis par le COR
ciblesTrans <- paste0(libDestinie,"parametres/Projection_COR_2019/PARAM_transitions.xls")
param_eco <- paste0(libDestinie,"parametres/Projection_COR_2019/ParamSociaux_2019.xls")

# remplissage de simul$macro selon le scénario considéré
# on merge toutes les feuilles excel du fichier qui nous intéressent en 1 seul data.frame stocké dans simul$macro
sheets = c("ParamGene","ParamRetrBase","ParamRetrComp","ParamRev","ParamAutres","ParamFam",paste0("cho",scencho,"_pib",scenprod))
simul$macro <- lapply(sheets, FUN = read_excel,path = param_eco, skip = 2)
simul$macro = (reshape::merge_recurse(simul$macro))%>%arrange(annee)  # NB: le passage en R 4.0 crée des soucis sur macro : annee n'est pas bien triée


simul$macro <- simul$macro%>%
  filter(annee%in%1900:fin_simul)%>%
  mutate( 
    SMPTp = ifelse(is.na(SMPTp),0,SMPTp),
    SMICp = ifelse(is.na(SMICp),0,SMICp),
    PIBp = ifelse(is.na(PIBp),0,PIBp),
    PlafondSSp = ifelse(is.na(PlafondSSp),0,PlafondSSp),
    Prixp = ifelse(is.na(Prixp),0,Prixp),
    MinPRp = 1.02,
    RevaloRG = ifelse(is.na(RevaloRG),1+Prixp,RevaloRG),
    RevaloFP = ifelse(is.na(RevaloFP),1+Prixp,RevaloFP),
    RevaloSPC = ifelse(is.na(RevaloSPC),1+Prixp,RevaloSPC)
  ) %>%
  projection( # quand les valeurs ne sont pas connues, on les projette de différente manière. Par ex: PIB en volume augmente selon l'hypo de croissance du PIB,
    SMPT ~ cumprod((1+SMPTp)*(1+Prixp)),
    PIB ~ cumprod((1+PIBp)*(1+Prixp)),
    PlafondSS ~ cumprod((1+PlafondSSp)*(1+Prixp)),
    SMIC ~ cumprod((1+SMICp)*(1+Prixp)),
    Prix ~ cumprod(1+Prixp),
    PointFP|PlafRevRG ~ SMPT,
    SalValid ~ SMIC,
    PlafARS1|PlafARS2|PlafARS3|PlafARS4|PlafARS5|PlafCF3|PlafCF4|PlafCF5|MajoPlafCF|sGMP|BMAF|SeuilPauvrete ~ SMPT,
    MaxRevRG ~ PlafondSS,
    MinPR ~ cumprod(MinPRp*(1+Prixp)),
    MinVieil1|MinVieil2|Mincont1|Mincont2 ~ lag(cumprod(1+Prixp)), # indexation standard. En évolution, indexation sur l'inflation de t-1.
    SalRefAGIRC_ARRCO|SalRefARRCO|SalRefAGIRC ~  cumprod(ifelse(annee%in%c(2016,2017,2018),(1+SMPTp+0.02)*(1+Prixp),
                                                                ifelse(annee%in%seq(2019,2033),(1+SMPTp)*(1+Prixp),(1+SMPTp-0.0116)*(1+Prixp)))
                                                         ),
    ValPtAGIRC|ValPtARRCO|ValPtAGIRC_ARRCO ~ cumprod(1+ifelse(annee%in%c(2016,2017,2018),pmax(Prixp-0.01,0),Prixp)),
    MinRevRG|SeuilExoCSG|SeuilExoCSG2|SeuilTxReduitCSG|SeuilTxReduitCSG2 ~cumprod(1+Prixp),
    .~1
  ) 

# NB: à l'étape au dessus, si message d'erreur, il faut recharger tout le package: devtools::load_all(libDestinie)

# remplissage de simul$CiblesTrans selon le scénario considéré:

simul$CiblesTrans <-  left_join(simul$macro %>% select(annee), 
                                xlsx::read.xlsx(ciblesTrans,
                                                sheetName = paste0("cho",scencho),
                                                startRow = 3,colIndex=1:36)
                                )
```

D'autres cibles économiques doivent être ajoutées à l'environnement _simul_. Ces cibles ne dépendent d'aucun scénario et auront toujours la même valeur. Ce sont les cibles de fin d'âge moyen de fin d'étude, les équations de salaire et de transitions sur le marché du travail, ainsi qu'un coefficient de correction de salaire selon l'âge et le sexe.



```r
simul$FinEtudeMoy <-  xlsx::read.xlsx(paste0(libDestinie,"parametres/PARAM_etude.xls"),sheetName = "TABLEFINDET0", startRow = 2)
simul$EqSalaires <-  xlsx::read.xlsx(paste0(libDestinie,"parametres/EqSalaires.xls"),sheetName="eq_salaires")
simul$CStructSexeAge <-  xlsx::read.xlsx(paste0(libDestinie,"parametres/EqSalaires.xls"),sheetName="CorrectStructSalSexeAge")

simul$EqTrans <-  xlsx::read.xlsx(paste0(libDestinie,"parametres/EqTrans.xls"),"EqTrans") %>%  
  mutate(indic=as.integer(indic)) %>%
  mutate(ordre=as.factor(paste0("TRANS",ordre+1))) %>%
  arrange(type_trans,origine,ordre)
```
#### Utilisateur extérieur: où trouver les fichiers de paramètres éco et démo?

La librairie Destinie 2 recense dans son dossier `data/` un ensemble de fichier de données _R_ correspondant à différentes séries d'hypothèses démographiques et économiques. Par exemple, le fichier `fec_Cent_vie_Cent_mig_Cent.Rda` est une liste avec tous les objets R correspondant aux hypothèses démographiques du scénario central-central-central.

```r
data("fec_Bas_vie_Cent_mig_Cent")
names(fec_Bas_vie_Cent_mig_Cent)
```

```
##  [1] "CiblesDemo"      "espvie_F"        "espvie_H"        "mortadiff_dip_F"
##  [5] "Survie_F"        "mortadiff_dip_H" "Qmort_F"         "Survie_H"       
##  [9] "Qmort_H"         "mortalite_diff"
```
 Ces fichiers ne sont en revanche pas les plus récents que nous disposons en interne. La crise récente du covid19 a par exemple sensiblement altéré les projections économiques du scénario 2020 du COR, et des simulations produites sur la base des jeux d'hypothèses librement disponibles dans ces fichiers ne tiendront pas compte de ces évolutions. Il pourrait être envisagé d'ajouter les fichiers les plus récents dont nous disposons lors des mises à jour annuelles du dépôt public de Destinie.


### Options de simulation
A ce stade, l'environnement _simul_ qui sera repris par le code source _C++_ est presque complet. Il reste à ajouter l'objet simul$options, qui contient une liste avec l'ensemble des options de simulation choisies par l'utilisateur.


```r
simul$options_salaires <- list()   # c'est une liste vide

# Liste des options: type de départ (tp), année max de législation (2020), pas de simulation (1/12), année de fin de simu, champ, options avancées
simul$options <- list("tp",anLeg=2020,pas1=1/12,pas2=1/12,
                      AN_MAX=as.integer(fin_simul),champ,
                      NoRegUniqAgircArrco=T, 
                      mort_diff_dip=T,
                      super_effet_hrzn=F
                      )
```


**L'environnement _simul_ est désormais prêt à l'emploi**: il détient de façon très structurée l'ensemble des informations dont Destinie a besoin pour lancer des simulations. Pour schématiser, _simul_ va être envoyé par _R_ au code source _C++_, qui va projeter les trajectoires des individus dans le temps et mettre à jour ce même environnement. La nouvelle version de _simul_ sera enfin réenvoyée dans l'environnement _R_.

Les chapitres suivants traitent des fonctions de simulation de Destinie. Quand cela nous semble nécessaire, le lien qui existe entre l'environnement _simul_ que nous avons constitué dans _R_ et les objets considérés dans le programme _C++_ sera détaillé.


___

