
# Module des transitions professionnelles {#chap4}


Ce chapitre porte sur le module des transitions sur le marché du travail de Destinie. Ce module consiste à simuler les trajectoires professionnelles de tous les individus en faisant évoluer leur statut d’activité et leurs trajectoires salariales. Ces dernières sont par ailleurs aussi simulées en rétrospectif. A la différence du module démographie qui est structuré dans une seule fonction appelée depuis _R_, trois fonctions _R_ constituent le module des transitions professionnelles: `destinieTransMdt()` pour les transitions de statut, et le couple `destinieImputSal()`,`destinieCalageSalaires()` pour l'imputation des salaires une fois les statuts simulés. Les trois fonctions sont définies dans des fichiers différents: `destinieTransMdt()` se trouve dans _Transitions.cpp/h_, `destinieImputSal()` est définie dans _Salaires.cpp/h_, et `destinieCalageSalaires()` est décrite dans _Destinie.cpp/h_.

 _____

## Transitions de statut professionel: _destinieTransMdt()_

Cette fonction va faire transiter les statuts professionnels chaque année. Elle renvoie ensuite dans l’environnement `simul` les tables `ech` et `emp`  mises à jour. `destinieTransMdt(simul)` est organisée principalement autour de la structure **_Transitions_** définie dans _Transition.cpp_. En effet, la fonction commence en créant une instance `trans` de **_Transitions_** et les transitions sont ensuite simulées à l’aide des méthodes de cette structure.


Pour simuler et appliquer les transitions sur le marché du travail, une estimation de probabilité par logits emboîté est conduite selon la méthodologie décrite dans [Bachelet et al. (2014)](https://www.insee.fr/fr/statistiques/1381043): selon le statut professionnel en `t-1` et le sexe, on estime la probabilité de transiter graduellement vers différents statuts au sein d’un arbre de transition. L’ordre de l’arbre de transition diffère selon le statut antérieur, mais pour des raisons de calage les trois premiers nœuds restent forcément les mêmes: 1) transition vers l’inactivité ; 2) transition vers le chômage ; 3) transition vers la fonction publique.

<div class="figure" style="text-align: center">
<img src="destinieTrans_schema.png" alt="&lt;b&gt;&lt;i&gt;Figure 1: Exemple d'un arbre de transition entre les différents statuts d'activité&lt;/i&gt;&lt;/b&gt;" width="80%" />
<p class="caption">(\#fig:unnamed-chunk-1)<b><i>Figure 1: Exemple d'un arbre de transition entre les différents statuts d'activité</i></b></p>
</div>

```cpp
// Fonction destinieTransMdt() sous une forme simplifiée  //

// a) Importation l'environnement simul de R et conversion vers les objets C++ (voir chapitre 2)
auto S = Simulation(envSim);
  Transitions trans(envSim); //création d'une instance trans de la structure Transitions (voir section suivante)

// b) Nettoyage: réinitialisation des statuts d'activité ultérieurs à 2009 et avant les études

// ... (partie omise) ... //
 

// c) Transitions sur le Mdt de 2010 (110) à l'année de fin de simulation

// en fonction de la présence de transNonCalees dans les options (par défaut, c'est actif), on mobilise la méthode appropriée pour lancer les transitions
  
  if(!options->transNonCalees) 
    for(int t : range(110,AN_NB)) // boucle de 2010 à l'année de fin de simulation
      trans.make_transitions(t,0,999);  
  else
    for(int t : range(110,AN_NB)) // boucle de 2010 à l'année de fin de simulation
      trans.make_transitions_noncalees(t,0,999);
      
     
// d) Réécriture des tables emp et ech et export dans R

// ex pour la table emp:
  static Rdout df_emp("emp", {"Id", "age", "statut", "salaire"});   // Nag: on exporte df_emp vers R, qui sera le dataFrame emp (on le remplit juste en dessous)
  for(Indiv& X : pop)  // pour tous les indiv..
    for(auto age : range(X.ageMax)) // ..et tous les ages jusqu'à l'ageMax
      if (X.Id != 0) {
        X.salaires[age] = (in(X.statuts[age],Statuts_occ)) ? 1 : 0;   // si le statut simulé à l'age courant correspond à un statut d'emploi, on fixe le salaire à 1, sinon à 0
		// Rq: les fonctions suivantes vont simuler les salaires des individus en emploi
        df_emp.push_line(X.Id, age, X.statuts[age], X.salaires[age]);  // on ajoute la ligne dans df_emp 
      }
```
  


### La structure **_Transitions_**, coeur de la fonction _destinieTransMdt(simul)_

La fonction `destinieTransMdt()` s'appuie principalement sur une méthode make_transitions() qui provient de la structure **_Transitions_**. Cette section décrit le fonctionnement de cette structure.

#### Attributs: 

- `EqTrans eqtd`: coefficients des variables explicatives des équations de transition. Ces coefficients sont stockés sous la forme d’une liste à 3 dimensions d’instances de la structure **_EqTrans_**, qui est une liste d’attributs correspondant à l’ensemble des coefficients des estimations ([Bachelet et al. 2014](https://www.insee.fr/fr/statistiques/1381043) que l’on retrouve dans l’objet _R_ `Simul$EqTrans`). On obtient les coefficients correspondant à un triplet (période_transition=x, origine=y, noeud_arbre=z), dans `eqtd[x][y][z]`;

- `CiblesTrans cibles`: cibles annuelles de taux de chômage, d'inactivité, de la part de la fonction publique (FP) dans l’emploi total et de la fonction publique d'Etat (FPE) dans la FP. Elles sont utilisées pour réaliser des transitions calées. Ces cibles annuelles par sexe et tranche d'âge sont stockées sous la forme d'une instance de **_CiblesTrans_**, simple copié-collé de la table `Simul$CibleTrans`.

- `vector2<double> probas` et `vector<double> probas_fpe`: probabilités de transition de chaque individu à chaque noeud de l'arbre de décision;

- `vector<int> champ`: pour chaque individu, une indicatrice de sortie de l'arbre de décision;

- `vector2<int> destination`: pour chaque individu et chaque noeud de l'arbre de décision, le statut de sortie éventuelle de l'arbre.

#### Méthodes, dans l'ordre d'usage de _destinieTransMdt()_: 

- `void ProbTrans(Indiv& X, int t)` : fonction calculant, pour un individu et une année, l'arbre de décision ainsi que les probabilités de transition dans chaque noeud de l'arbre;

- `void make_transitions(int t, int age_min, int age_max)`: fonction simulant, avec calage, les transitions d'une année;

- `void make_transitions_noncalees(int t, int age_min, int age_max)`: fonction simulant, sans calage, les transitions d'une année;

- `void applique_trans(int id, int t, int position_arbre)`: fonction qui applique effectivement la transition à un individu, en remplissant le nouveau statut;

- `void transMigr(Indiv& X, int age)`: fonction qui simule la transition pour un migrant lors de son arrivée en France.


#### Focus sur la méthode principale _trans.make_transitions(t)_

`make_transitions(t)` va simuler les transitions sur le marché du travail et met à jour les attributs de tous les individus selon l’issue de ces simulations. Cette ‘supra-méthode’ fait ainsi appel aux autres méthodes de la structure _**Transitions**_. Son organisation peut se résumer comme ci-dessous:

1. Calcul des probabilités de transition pour tous les individus, et pour tous les nœuds (8) de leur arbre de décision par le biais de la méthode `ProbTrans(X,t)`. Cette méthode détermine les probabilités individuelles selon les coefficients d’un modèle logit avec différentes variables explicatives, la valeur des coefficients d’estimation utilisés (stockés dans _eqtd_, duplicat de `simul$eqTrans`) étant différente selon le statut d’origine, le sexe et la période dans la carrière (début, milieu, fin).

2. Une fois les probabilités calculées, des tirages successifs sont menés du premier au dernier nœud afin de définir les individus qui vont effectivement transiter. Si un individu transite à un certain nœud, celui-ci n’est plus candidat à la transition vers les nœuds suivants. La méthode `applique_trans()` qui met à jour le statut d’activité est alors appelée pour ces individus.

On peut également noter que les probabilités de transition pour les migrants sont calculées différemment, à l’aide de la méthode `transMigr()` qui donne des probabilités de transition, tire les transitions et les applique.



## Simulation des salaires: _destinieImputSal()_ et _destinieCalageSalaires()_

### _destinieImputSal()_

Cette fonction va attribuer des revenus d’activité aux individus, en se servant des statuts d’activité simulés plus tôt dans la fonction `destinieTransMdt()`. Les trajectoires salariales sont également simulées en rétrospectif. La procédure d’estimation est décrite dans [Aubert, Bachelet 2012](https://www.insee.fr/fr/statistiques/1381012). Elle renvoie dans l’environnement _R_ les tables _ech_ et _emp_ mises à jour. Définie dans le fichier _Salaires.cpp/h_, `destinieImputSal()` est organisée principalement autour de la structure **_Salaire_** décrite plus bas.


```cpp
// Fonction destinieImputSal() sous une forme simplifiée  //

// a) Importation l'environnement simul de R et conversion vers les objets C++ (voir chapitre 2)
 auto S = Simulation(envSim);

// b) Nettoyage: réinitialisation des statuts cadres/non-cadres, création d'aléas individuels pour l'imputation des salaires

// ... (partie omise) ... //
 
// c) Simulation des salaires perçus chaque années pour tous les individus

  auto sal = Salaire(envSim); // création d'une instance de la structure Salaire décrite dans la section suivante
    
    for(Indiv& X : pop) 
   { // pour tous les individus de la population
        double e1 = aleas[0][0][X.Id];  // génération d'aléas individuels (voir Aubert, Bachelet 2012)
        double e2 = aleas[0][1][X.Id];
        
        for(int age : range(13,X.ageMax+1))
       { // de 13 ans à l'ageMax
            if(in(X.statuts[age], Statuts_occ)) { // si actif occupé
                double e3 = aleas[age][0][X.Id];
                double e4 = aleas[age][1][X.Id];
                
                X.salaires[age] = sal.imput_sal(X, age, e1, e2, e3, e4);  // méthode d'imputation des salaires (décrite plus bas)
                
            } else {  //pas de salaire si non actif occupé 
                X.salaires[age] = 0;
            }
        }
    }
    
 // d) Imputation de l'avpf, du statut de cadre et du taux de prime des fonctionnaires
    
    for(int t : range(71,AN_NB))  //  de 1971 à la fin de la simu
      sal.imput_avpf(t);  // imputation de l'avpf
      
    for(int t : range(AN_NB))  // tous les ans
      sal.imput_statut_cadre(t); // imputation du statut de cadre
   
      sal.input_taux_prim();  // imputation du taux de prime des fonctionnaires
     
// e) Réécriture des tables emp et ech et export dans R

// ex pour la table emp:
    for(auto& X : pop) for(auto age : range(0,X.ageMax+1))  // pour tous les indiv X et tous les ages 
         { 
          if (X.Id == 0) continue;
          static Rdout df("emp", {"Id","age","statut","salaire"}); on exporte df_emp vers R, qui sera le dataFrame emp (on le remplit juste en dessous)
          df.push_line(X.Id, age, X.statuts[age], X.salaires[age]); on ajoute la ligne correspondant à l'individu X à l'âge age dans df_emp 
          }
```

### La structure **_Salaire_**, coeur de la fonction _destinieImputSal()_

La fonction `destinieImputSal()` s'appuie principalement sur principale `imput_sal()` et les méthodes complémentaires `imput_avpf()`, `imput_statut_cadre()`, `input_taux_prim` qui proviennent de la structure **_Transitions_**. Cette section décrit le fonctionnement de cette structure.

#### Attributs: 

- `EqSalaires esq`: coefficients des variables explicatives des équations de salaire. Ces coefficients sont stockés sous la forme d’un vecteur d’instances de la structure **_EqSalaires_**, qui est une liste d’attributs correspondant à l’ensemble des coefficients des estimations que l'on retrouve dans l'objet _R_ `simul$EqSalaires`. Il y a 6 instances pour 6 jeux de coefficients différents utilisés selon le sexe et le type de secteur (privé, indépendant, FP);

- `CstructSexeAge cs`: contient une série de coefficients correcteurs utilisés lors de la simulation des salaires, pour les femmes et les hommes. C’est une instance de la classe **_CstructSexeAge_** (duplicat de `simul$CStructSexeAge`): table par année avec une colonne _CORREC_HOMMES_ et une colonne _CORREC_FEMMES_;

- `Options_salaires options_sal`: contient un attribut booléen valant 1 pour une simulation dans salaires sans aléa, 0 sinon. C'est une instance de **_Options_salaires_**  (duplicat de `simul$options_salaires`).


#### Méthodes, dans l'ordre d'usage de `destinieImputSal()`

- `double imput_sal(Indiv& X, int age,  double ei1, double ei2, double e1, double e2)`: simule le revenu d'activité de l'individu _X_ à l'âge _age_, en fonction du sexe et du statut;

- `void imput_avpf(t)`: impute le statut d'AVPF en l'année _t_. Plusieurs sous-méthodes sont mobilisées: `double revenuCoupleNet( Indiv& X,int age)`, `int prestafam(Indiv& X, int age)`, `bool test_avpf(Indiv& X, int age)`;

- `void imput_statut_cadre(t)`: permet de choisir parmi les salariés du privé en _t_ ceux qui seront considérés comme cadre (_S_CAD_) et ceux qui sont considérés comme non-cadres (*S_NC*). Pour ce faire, on choisit cette proportion selon une cible exogène lissée par année (de 5% en 1930 à 25% en 2050+), on trie les salariés selon leur salaire imputé et on fixe comme cadre les salariés avec la plus haute rémunération;

- `void input_taux_prim()`: impute un taux de prime pour les fonctionnaires. Ce taux est supposé constant tout le long de la carrière. Pour cela, la méthode va déterminer le dernier âge correspondant à un statut de fonctionnaire, et calculer le salaire perçu à cette date relativement au salaire moyen par tête à l’année correspondante. Le taux de prime imputé sera alors tiré dans une distribution selon la valeur du salaire relatif et le sexe de l’individu.

#### Focus sur la méthode principale _sal.imput_sal()_

`imput_sal(t)` est la méthode qui va simuler les salaires des individus chaque année. Son programme simplifié est décrit plus bas, mais son fonctionnement peut se résumer simplement sous ces deux étapes:

1.	Un premier salaire individuel est simulé par des équations de salaires (régression linéaire) où 6 jeux de coefficients sont utilisés selon le sexe et le type de statut professionnel. Les variables explicatives, créées dans la fonction, portent principalement sur l’ancienneté (absolue et dans le statut) et l’âge de fin d’études.

2.	Pour générer davantage de variabilité dans les salaires prédits, une procédure additionnelle décrite dans [Aubert, Bachelet 2012](https://www.insee.fr/fr/statistiques/1381012) est lancée. Celle-ci consiste à simuler un incrément aléatoire au salaire simulé dans l’étape précédente pour générer davantage de variabilité dans les rémunérations, et a fortiori dans les pensions de retraite simulées dans Destinie.

```cpp
  // Principe de la méthode sal.imput_sal //

  if(age >=55 && X.salaires[age-1] != 0)
    return X.salaires[age-1] * M->SMPT[t] / M->SMPT[t-1];  // Nagui: a 55+, on revalorise chaque année le salaire par le SMPT
  
  int typeSal =   // on place les individus dans une case selon le sexe et le statut
    in(X.statuts[age],{1,2,11})                ? (X.sexe == HOMME ? PRI_H : PRI_F) : 
    in(X.statuts[age],{311,312,321,322}) ? (X.sexe == HOMME ? PUB_H : PUB_F) :
    (X.statuts[age]==4)                     ? (X.sexe == HOMME ? IND_H : IND_F) : -99;
	
  auto& eq = eqs[typeSal];     // eq donne les coefficients d'estimation correspondant au typeSal de l'individu X
  
  // Création des variables explicatives nécessaires à l'estimation du salaire: //
  
  int findet = min_max(X.findet, 14, 26) - 14;  // on normalise l'âge de fin d'études
  int findet2 = findet*findet;
  double gen = min_max((X.anaiss-1935)/35.0,0,1);  // indice générationnel, qui vaut entre 0 et 1 selon l'année de naissance
  if(options->SalNoEffetGen) gen = 0;  // si pas d'effet générationnel, gen est remis à 0
  double adferelatif =  - 5    // age de fin d'études relatif selon la génération
  + 0.641          * findet
    + (1-0.641)/12.0 * findet2
    + 3.961          * (1-gen) 
    + 0.0238         * findet * (1-gen)
    - 0.0296         * findet2 * (1-gen);
        
    double stock_dur_statut = duree_statut(X,age);  // durée dans le statut
    double stock_dur_emp = duree_emp(X,age);  // durée en emploi
    
    double stock_dur_statut6 = max(0.0,6.0-stock_dur_statut);  // 6-durée dans le statut -> fait 0 si 6 ans ou +, et va jusqu'à 6 
    double stock_dur_emp6 = max(0.0,6.0-stock_dur_emp);  // idem pour durée en emploi
    double stock_dur_statut2 = stock_dur_statut*stock_dur_statut;  //vars au carré
    double stock_dur_emp2 = stock_dur_emp*stock_dur_emp;
    
    // création des aléas individuels:  //
    double alea_indiv1 = min_max(alea_norm1(ei1,ei2), -3, 3); // ei1 et ei2 sont independantes et de loi uniforme sur [0,1] => Box-Müller s'applique. 
    double alea_indiv2 = min_max(alea_norm2(ei1,ei2), -3, 3);

	// Estimation du salaire selon les valeurs des var. explicatives et des coefficients dans eq
    double xbeta =                                      
      eq.Intercept +
      (stock_dur_statut == 1)             *  eq.E_1er     +
      stock_dur_statut6                   *  eq.E_0       +
      stock_dur_statut                    *  eq.E         +
      stock_dur_statut2                   *  eq.E2        +
      stock_dur_statut  * adferelatif     *  eq.FR_E      +
      stock_dur_statut2 * adferelatif     *  eq.FR_E2     +
      stock_dur_statut6 * adferelatif     *  eq.FR_E_0    +
      stock_dur_emp2    * adferelatif     *  eq.FR_D2     +
      stock_dur_emp                       *  eq.D         +
      stock_dur_emp2                      *  eq.D2        +
      stock_dur_emp * adferelatif         *  eq.FR_D      +
      stock_dur_emp6                      *  eq.D_0       +
      stock_dur_emp6 * adferelatif        *  eq.FR_D_0    +
      adferelatif                         *  eq.FR        ;
  
    
      double correct_fp =  // Correction du salaire selon le statut (FPE v.s. FPTH v.s. autres)
      (in(X.statuts[age],{311,321})) ? 1.1 :
      (in(X.statuts[age],{312,322})) ? 0.9 :
      1.0 ;
      
    // Simulation d'un niveau et d'une pente individuelle selon leurs moyennes, variances et correlations //
    
    double estpente = eq.meanpente_indiv + eq.Sig2pente_indiv * alea_indiv2;
    
    double estniv = 
      eq.mean_indiv +
      eq.corr_indiv * eq.Sig2_indiv * alea_indiv2 +
      eq.Sig2_indiv * sqrt(1-eq.corr_indiv*eq.corr_indiv) * alea_indiv1;
   
    double variance_an = 
      eq.Sig2_an +
      (stock_dur_statut == 1)                * eq.res_E_1er  +
      stock_dur_statut6                      * eq.res_E_0    +
      stock_dur_statut6 * adferelatif        * eq.res_FR_E_0 +
      stock_dur_emp                          * eq.res_D      +
      stock_dur_emp2                         * eq.res_D2     +
      stock_dur_emp6                         * eq.res_D_0    +
      stock_dur_emp6 * adferelatif           * eq.res_FR_D_0 +
      adferelatif                            * eq.res_FR ;
    
    double ecarttype_an = sqrt( max(0.0, variance_an)) ;
    double eps = min_max(alea_norm1(e1,e2), -2.0, 2.0) * indic_alea; 

    // Calcul du salaire //

    double ln_w = xbeta + estpente*stock_dur_emp + estniv + ecarttype_an*eps;
    double correctStruct = (X.sexe==1) ? cs.CORREC_HOMMES[t] : cs.CORREC_FEMMES[t];
    double salaire = exp(ln_w) * M->SMPT[t] * correct_fp;
}  

```

### _destinieCalageSalaires()_

Cette fonction sert à caler les salaires imputés dans `destinieImputSal()` pour respecter la trajectoire du Salaire Moyen Par Tête (SMPT). Le calage est donc réalisé en évolution et pas en niveau: les salaires simulés croissent de la même manière que le SMPT mais n'ont pas nécessairement la même valeur. En sortie, `destinieImputSal()` retourne dans _R_ le même environnement, dont _emp_ avec des salaires modifiés. 

C'est également dans cette fonction que l'éventuel effet d'horizon est implémenté: celui-ci consiste à prolonger l'âge de fin d'activité des seniors des générations récentes pour tenir compte de l'impact du décalage de l'âge d'ouverture des droits induit par les réformes de retraite récentes, notamment celle de 2010.

```cpp
// Fonction destinieImputSal() sous une forme simplifiée  //

// a) Importation l'environnement simul de R et conversion vers les objets C++ (voir chapitre 2)
 auto S = Simulation(envSim);
 
// b) Effet horizon (outils pour tenir compte de l'impact des réformes de retraite): 
    // -> effet_hrzn allonge l'âge de fin d'activité de jusqu'à +2 ans à partir de la gen 1951
    // -> super_effet_hrzn allonge l'âge de fin d'activité de jusqu'à +4 ans
  
  // Exemple pour effet_hrzn:
  if(options->effet_hrzn) 
 { 
    for(Indiv& X : pop) {
      if(X.anaiss >= 1951) {  // pour tous les individus nés après 1950
        int agefinAct = 0;
        
        for(int age : range(min(70,X.ageMax)))  // de la naissance à l'age maximal (écrété à 70 ans)
          if(in(X.statuts[age],Statuts_occ))   // si dans un statut d'occupation
            agefinAct = age;  // on fixe l'age de fin d'activité à cet age là
        
        if(agefinAct >= max(55,2010-X.anaiss)) {  // si fin d'activité après  2010 (55 pour gen 55, 56 pour gen 54, 57 pour gen 53, 58 pour gen 52, 59 pour gen 51)
          double decalage = 2 * partTx(X.anaiss, 1951, 1955);  
		  // partTx donne la proportion de l'année de naissance compris entre 1951 et 1955: 0 si avant 51, 1 si après 55, (dateNaiss-1951)/(51-55) sinon
		  // decalage = 0 si avant 51, 2 si après 55, et progressivement jusqu'à 2 avec les gen 52 -> 54 (=> c'est une manière d'appliquer la règle progressivement)
		  
          int agefinActNouv = agefinAct + int(decalage + alea()); // le nouvel age de fin d'activité = age fin d'act observé + decalage compris entre 0 et 2
          
          if(agefinAct+1 <= agefinActNouv && X.statuts[agefinAct+1] != 0) {  // si l'age de fin d'activité à été augmenté d'au moins 1 ans, et si le statut d'activité agefinAct+1 est non nul
            X.statuts[agefinAct+1] = X.statuts[agefinAct];  // -> on prolonge le statut d'activité T+1 par celui observé en T
            X.salaires[agefinAct+1] = X.salaires[agefinAct] * M->SMPT[X.date(agefinAct+1)] / M->SMPT[X.date(agefinAct)]; // on prolonge le salaire tout en corrigeant du SMPT
          }
          if(agefinAct+2 <= agefinActNouv && X.statuts[agefinAct+2] != 0) {  // idem si age décalé de 2 ans: on prolonge de 2 ans le dernier statut
            X.statuts[agefinAct+2] = X.statuts[agefinAct];
            X.salaires[agefinAct+2] = X.salaires[agefinAct] * M->SMPT[X.date(agefinAct+2)] / M->SMPT[X.date(agefinAct)];
          }
        }
      }
    }
  }
// c) Calage des salaires sur l'évolution du SMPT

  double ref(1);
   for(int t : range(50,AN_NB))  // boucle sur chaque année
{  // de l'année 50 à la dernière année de simu

      double som(0), nb(0), som55(0), correct(1);
      
      // Calcul de la sommes salaires avant corrections

      for(Indiv& X : pop)   if(X.est_present(t) && X.age(t) > 14) 
      {  // pour tous les individus présents de + de 14 ans
          int age = X.age(t);
          
          // test de liquidation  (on veut savoir qui est retraité, pour que le calage soit effectué uniquement sur les actifs en t)
          if(age >= 50 && !X.retr->totliq) {
            X.retr->SimDir(age);  // si l'individu n'a pas encore liquidé, on lance SimDir()) pour avoir les infos de retraite (détaillé dans le chap 5)
          }
          	  	 
          X.retr->revaloDir(t);  // revalorisaton des pensions de retraite (détaillé dans le chap 5)
          
          double ind_senior1 = (age>=55) *     // ind_senior_1 = si age>=55 ans, c'est la part travaillée avant la liquidation (ex: si liquidation en juin: 6/12)
              statut(X,age,Statuts_occ) *      // on met age et age-1 pour gérer les cas où le mois de liquidation est après ou avant l'anniversaire
              statut(X,age-1,Statuts_occ);
          
          double ind_occ = statut(X,age,Statuts_occ);  // ind_occ c'est la part travaillée avant liquidation, quel que soit l'age
		  // rq: ce sera toujours 1 pour les occupés, et une fraction pour ceux qui ont liquidé à l'année en question
	
          nb  += ind_occ;  // nb d'individus sous statut d'occupation proratisé par le nb de mois travaillés
          som += (ind_occ-ind_senior1) * X.salaires[age];  // on fait le compte des occupés, hors seniors de + de 55 ans
          som55 += ind_senior1 * X.salaires[age-1] * M->SMPT[t] / M->SMPT[t-1];  // pour les + 55 ans, on prend le salaire précédent que l'on revalorise selon le SMPT
		  // pour les seniors de 55+, on revalorise simplement le salaire selon le SMPT (il n'y aura pas de transformation en dessous pour eux)
		 }
		 
      // Calcul du coefficient de correction de l'année  (calage en évolution à partir de l'année de base)
      correct = (t <= AN_BASE) ? 1   : ((M->SMPT[t] * nb * ref - som55) / som);  // correct est une cible d'évolution du salaire moyen pour les - de 55ans
      ref     = (t != AN_BASE) ? ref : ((som + som55) / nb) / M->SMPT[t];  // à partir de 2009 (AN_BASE), ref ne vaut plus 1, mais vaut (salaire moyen obervé / SMPT) 
    
      // Application du coefficient de correction
      for(Indiv& X : pop) if(statut(X, X.age(t), Statuts_occ)>0)
      {
          int age = X.age(t);
          X.salaires[age] = (age >= 55 && statut(X, age-1, Statuts_occ))  ?
                            X.salaires[age-1] * M->SMPT[t] / M->SMPT[t-1] :
                            X.salaires[age] * correct ;
      }
}
// d) Réécriture de la table emp à exporter dans R:
  for(Indiv& X : pop) for(auto age : range(X.ageMax)) 
  {
      if (X.Id == 0) continue;
      if (!in(X.statuts[age],Statuts_occ))
        X.salaires[age] = 0;
      static Rdout df_emp("emp",{"Id","age","statut","salaire"});
      df_emp.push_line(X.Id,age,X.statuts[age],X.salaires[age]);
  }
```
