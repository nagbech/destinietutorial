


# Module des retraites {#chap5}

Ce chapitre porte sur le module des retraites de Destinie. Ce module va calculer les pensions de retraites et de réversion des individus de la date de liquidation à la date de décès. La date de jouissance, le niveau de liquidation et des revalorisations successives dépendent de plusieurs options de simulations: principalement l’option de départ à la retraite (la référence étant le départ au taux plein) et la dernière législation en vigueur. Construite sur un logique de programmation orientée objet, cette fonction fait appel à plusieurs structures qui ont des objectifs différents: test de liquidation, application de la législation, revalorisations. Les structures concernées, _**Retraites**_, _**DroitsRetr**_, _**Legislation**_ et _**Reversion**_ sont détaillées dans le [chapitre 2](chap2.md). La fonction `destinieSim()`, exportable depuis R, est définie dans le fichier _Destinie.cpp/.h_.
 
 _____

## Organisation de la fonction _destinieSim()_

```cpp
// Fonction destinieSim() sous une forme simplifiée  //

// a) Importation l'environnement simul de R et conversion vers les objets C++ (voir chapitre 2)
  auto S = Simulation(env);

 // b) Coeur de la fonction, découpé en 2 blocs
 
 for(int t=50; t <= AN_FIN; t++) 
 {      // boucle annuelle à partir de 1950
 
 // Droits directs
   for(Indiv & X: pop)   // boucle sur chaque individu X présent l’année t
        if(X.est_present(t)) 
        {
          if(X.age(t) >= 50 && !X.retr->totliq) {  // Si âge >=50 ans, et si pas encore de liquidation totale
              X.retr→SimDir(X.age(t));} // => On lance la méthode SimDir adossée à l'attribut retr de l'indiv (instance de Retraites)
         
          X.retr->revaloDir(t);  // Pour tous les indiv, on revalorise les droits directs
                                 // rq: Si liquidation pas effectuée, les attributs de la pension restent à 0
        }

 // Droits dérivés (réversion)
   for(Indiv & X: pop)
        if(X.est_present(t)) 
        {
          int age = X.age(t);
          r_assert(X.conjoint[age] >= 0 && X.conjoint[age] <= int(pop.size()));  // si l’individu X a un conjoint Y l’année t
          Indiv& Y = pop[X.conjoint[age]];
if(Y.est_present(t-1) && !Y.est_present(t) &&Y.emigrant==0) {  // N: si le conjoint Y est décédé entre t-1 et t
              X.retr->SimDer(Y,t);  // N: on lance la méthode SimDer (simu des droits dérivés) adossée à l'attribut retr de l'individu
          }
          else  if(t==AN_BASE &&X.conjoint[age]>0 && X.matri[age]==VEUF ) { // si lors de l’année de base (2010) l’individu est catégorisé comme veuf, on lance aussi SimDer()
              X.retr->SimDer(Y,t);
          }
          X.retr->revaloDer(t); // Pour tous les indiv, on revalorise les droits dérivés
                                 // rq: Si liquidation pas effectuée, les attributs de la réversion restent à 0
        }
        
  // c) Autres procédures:      
  
  // minimulm vieillesse
   for(Indiv & X: pop)
        if(X.est_present(t)) 
        {
           X.retr->min_vieil=0;
          if(X.age(t) >=65 && X.est_persRef(t))
              X.retr->minvieil(t); // méthode qui calcule le min. vieillesse et le revalorise chaque année
        }
        
    // d) Export vers des objets R:
    
       ecriture_retraites(t);            // simul$retraites
       ecriture_indicateurs_an(t);       // simul$indicateurs_an
       ecriture_indicateurs_gen_age(t);  //simul$indicateurs_an
 }
   // ... //
   ecriture_liquidations(); // simul$liquidations
   // ... //
   ecriture_cotisations(); // simul$cotisations
   ecriture_salairenet();  // simul$salairenet
```

## Focus sur _SimDir()_ et _SimDer()_, coeur de la fonction _destinieSim()_

`destinieSim()` fait appel aux méthodes `SimDir()` et `SimDer()` depuis l'attribut de l'individu nommé `retr`, instance de la structure _**Retraites**_. Ces méthodes sont codées selon des logiques différentes en puisant dans différentes structures: _**DroitsRetr**_ et _**Legislation**_ pour `SimDir()`, _**Reversion**_ et _**Legislation**_ pour `SimDer()`. L'ensemble de ces structures est décrit dans le [chapitre 2](chap2.md). Notons enfin que deux autres méthodes sont également importantes pour comprendre intégralement la fonction `destinieSim()`: ce sont les méthodes de revalorisation des droits directs (`revaloDir()`) et dérivés (`revaloDer()`). Appelées chaque année dans la boucle de `destinieSim()`, elles ne sont pas présentées en détail dans ce chapitre mais peuvent être consultés dans le code source de la structure _**Retraite**_.

### Droits directs: la méthode _SimDir()_

```cpp
// Méthode SimDir() de la structure Retraite, sous une forme simplifiée  //

// a) création d'une instance l de la structure Legislation selon l'individu X et son âge

Leg l = Leg(X, age, min(X.anaiss+age,options->anLeg)); // -> donne la législation en vigueur pour X à l'âge du test
                                                       // NB: la dernière législation est renseigné dans l'option anLeg
                                                       
// b) définition de l’age du test selon le pas mentionné dans les options 

    double pas = (age < 55) ? options -> pas2 : // pas de test: pas2 en dehors de la fenêtre de liquidation 
                 (age > l.AgeAnnDecRG) ? options->pas2 : // pas1 dans la fenêtre de liquidation (en général: mois) 
                                         options->pas1 ;
                                         
    // ageLegTest/moisTest: âge (et mois) à partir duquel on fait des test de liquidations                                     
    double ageLegTest = 
    (age < 55) ? l.AgeMinFP :  // avant 55, test seulement pour les actifs de la FP -> on considère ageMin de FP 
    (age > l.AgeAnnDecRG) ? l.AgeAnnDecRG : // si âge > ageTP sans liquidation (ex: arrivée migrant à 68 ans),
                                            // on fixe âge artificiel = l'AAD pour faire liquider automatiquement
     l.AgeMinRG    ;                        // autres cas: on initialise ageLegTest à l'ageMin du RG
    
    int moistest = arr(12*ageLegTest + X.moisnaiss+1) % arr(12*pas);  // mois de l'année où s'effectue le test 
		// (en pratique, c'est un mois si le pas est mensuel, et un age en année si le pas est annuel)
    
    double agetest  = arr_mois(age, moistest - X.moisnaiss - 1);  // arrondi mois de l'age lors du test

    if(age > 70)    
        return;
    
    double ageMax;
    
    // boucle infra-annuelle pour tester la liquidation
    while(moistest < 12 && !totliq)
  {
      
        // on teste la première, et éventuellement la seconde liquidation
        ptr<DroitsRetr> dr = make_shared<DroitsRetr>(X,l,agetest); // instance de DroitsRetr pour X, leg l, à l'agetest
        
        ageMax = dr->AgeMax();
		
        if(!primoliq && TestLiq(X,*dr, l, agetest)) {  // si pas de primoliquidation, et TestLiq concluant (booléen) => primoliquidation
            primoliq = dr;  // dr est stocké dans primoliq, pour photographier l'état de DroitsRetr lors de la primoliquidation
            liq = dr;       // idem pour liq
            totliq   = (dr->liq) ? dr : NULL;    // dr stocké dans totliq si liq=true
            if(options->ecrit_dr_test) ecriture_droitsRetr(X,*dr);  // ecriture des droits, si spécifié en option
		    }
        else if(primoliq && TestSecondLiq(X, *dr, l, agetest)) {  // si primoliquidation déjà faite et TestSecondLiq concluant => liquidation totale
            totliq = dr;  //  dr est stocké dans totliq
            liq = dr;      //  dr est stocké dans liq
            if(options->ecrit_dr_test) ecriture_droitsRetr(X,*dr);
        }
        else {  // Si aucune liquidation n'est attendue à cette date => pas de maj de primoliq/totliq/liq
            if(options->ecrit_dr_test) ecriture_droitsRetr(X,*dr);
        }
        
        moistest +=    // incrémentation du moistest vers le mois suivant
            (arr_mois(l.AgeMinFP    - agetest) > 0)  ? arr(12*min(pas, l.AgeMinFP   - agetest)) :
            (arr_mois(l.AgeMinRG    - agetest) > 0)  ? arr(12*min(pas, l.AgeMinRG   - agetest)) :
            (arr_mois(l.AgeAnnDecRG - agetest) > 0)  ? arr(12*min(pas, l.AgeAnnDecRG- agetest)) :
            (arr_mois(ageMax        - agetest) > 0)  ? arr(12*min(pas, ageMax     - agetest))   :
                                                       arr(12*pas);
        
        agetest  = arr_mois(age, moistest- X.moisnaiss - 1);  // maj de l'agetest mtn que moistest a été modifié
    } // fin boucle infra-annuelle
    
	// Revalorisation des retraites à l'age age (0 vers 0 si pension pas encore liquidée)
    revaloDir(X.date(age)); // Permet noramment de mettre à jour les attributs de pension de X.retr

```
#### Principe de fonctionnement de _TestLiq()_ pour tester la liquidation des droits directs


La méthode `SimDir()` fait appel dans sa boucle infra-annuelle à deux fonctions majeures : `TestLiq()` et `TestSecondLiq()` qui ont pour objectif de déterminer si l'individu X à l'âge du test peut partir à la retraite, compte tenu des droits à la retraite cumulés _dr_ (instance de la structure *__DroitsRetr__*), l’option du type de départ (par défaut: à l'atteinte du taux plein) et la législation l (instance de la structure *__Leg__*). Ces deux fonctions sont définies dans _Outilscomp.cpp/.h_. Deux tests de liquidations sont nécessaires pour les cas d'individusde la fonction publique ayant au cours de leur carrière cotisé à un régime privé. 

Ci-dessous, la logique de construction de la fonction `TestLiq()` est expliquée.

```cpp
// Fonction bool TestLiq(Indiv & X, DroitsRetr & dr, Leg &l, sous une forme simplifiée  //

    int agearr = int_mois(agetest, X.moisnaiss+1);

// a) Gestion des cas mécaniques
    // pas de liquidation si age non atteint ou si durée cotisée = 0
    bool agemin = dr.AgeMin(); // 1 si age minimum atteint (c'est une méthode, car l'agemin est individuel)
    if(dr.duree_tot==0 || !agemin) {  return false; }
    
    // Liquidation d'office si age max atteint
    if(arr_mois(agetest) >= arr_mois(dr.AgeMax())) {
        dr.Liq();  // méthode qui enclenche la liquidation (met à jour les attributs de la pension de dr)
        return true; }
    
    // liquidation d'office si chomeur ou pré-retraite ayant atteint le taux plein
    static vector<int> statuts_prcho = {S_CHO,S_PR}; 
    if(in(X.statuts[agearr], statuts_prcho) && agetest >= l.AgeMinRG)  // si chomage/pré-retraite avec age > min du R.G
    { 
        if( (dr.duree_fp==0 && dr.tauxliq_rg>=1) || (min(dr.tauxliq_rg, dr.tauxliq_fp) >= 1))   // si atteinte du TP
        {
            dr.Liq();
            return true;
        }
    }
    
    // Liquidation d'office si invalidité
    static vector<int> statuts_invalid = {S_INVAL,S_INVALRG,S_INVALIND};
    if(in(X.statuts[agearr], statuts_invalid) && agetest >= l.AgeMinRG) // si invalide avec age > min du R.G
    { 
        dr.Liq();
        return true;
    }

    return false;  // dans les autres cas: le test de liquidation retourne faux
```

La méthode `Liq()`, instance de la structure *__DroitsRetr__*, est appelée si le test de liquidation est concluant. Cette méthode met à jour les attributs de la pension tels que _pension_rg_, _pension_fp_ et _ageliq_.

```cpp
// Méthode Liq() de la structure DroitsRetr, sous une forme simplifiée  //

// pension à 0 si pas de liquidation (age < min, cotisations nulles)
if (arr_mois(agetest) < min(arr_mois(l.AgeMinFP), arr_mois(l.AgeMinRG))  || (duree_tot_maj == 0))
   {
      pension_rg     = 0;
      pension_ar     = 0;
      pension_ag     = 0;
      pension_in     = 0;
      pension_fp     = 0;
      pension        = 0;
      VFU_rg         = 0;
      VFU_ar         = 0;
      VFU_ag         = 0;
      agefin_primoliq= 0;
      agefin_totliq  = 0;
	    pension_ag_ar  = 0;
   }
   

   //# Calcul des variables intermédiaires et de la distance au TP
   //# RQ : le calcul des durées et du taux de liq. a déjà été effectué lors de l'appel de AgeMin() dans TestLiq()
   SalBase(); // Calcule les salaires servant de base au calcul des pensions de base
   Points();  // Calcule les nombres de points accumulés dans les régimes ARRCO et AGIRC
   
   
   
   if (duree_fp==0){
      pension_fp    = 0; // pension FP nulle si pas de cotisation
   } else   {   
      LiqPublic();  // calcul de la pension FP si cotisation FP > 0 ( = taux*prorat*salaireReference)
   }
   
   if ( arr_mois(agetest) < arr_mois(l.AgeMinRG) ||  // conditions selon lesquelles la pension RG = 0
         (
            (options->SecondLiq || options->anLeg<2014 ||
            int_mois(X.anaiss+agetest, X.moisnaiss+1)<2015 || duree_fp==0
            ) && options->tp && tauxliq_rg<1 // taux RG < TP
          )
      ) 
   {
      pension_rg     = 0;
      pension_ar     = 0;
      pension_ag     = 0;
      pension_in     = 0;
      VFU_rg         = 0;
      VFU_ar         = 0;
      VFU_ag         = 0;
	  pension_ag_ar  = 0;
   }
   else    
      LiqPrive();  // sinon, calcul de la pension RG
      
 
   // Application du minimum contributif (ou garanti)
   min_cont = 0;
   min_cont_in = 0;
   if (!options->NoMC || !options->NoMG)    
      AppliqueMin();
	  
   // Application des bonifications pour 3 enfants
   if (!options->NoBonif && (X.nb_enf(age)>=3))    
      AppliqueBonif();

	  
coeffTempo();  // coefficient temporaire décote AGIRC-ARCCO
  

//  Mises a jour finales  //
   pension            =    pension_rg + pension_ar + pension_ag + pension_ag_ar +
                             pension_fp + pension_in ;  // pension totale
   
   if  ( pension > 0 && (duree_rg + duree_in>0) && (pension_rg + pension_in + VFU_rg==0) )
{
      // primo-liquidation:
      ageprimoliq        = age;
      ageliq             = 0;
      agefin_primoliq    = agetest;
      agefin_totliq      = 0;
      pliq               = 0; 
      primoliq           = true;
      liq                = false;
      dar                = primoliq && dar  ;
	  
//...//

	// Avant la réforme de 2014, le versement de cotisations après la primo-liquidation permet d'acquérir de nouveaux si elles sont versées à un régime ne versant pas la primo-pension.
	if(options->anLeg<2014 || int_mois(X.anaiss+agetest, X.moisnaiss+1)<2015) {
	for(int a=int_mois(agetest, X.moisnaiss+1)+1; a<=X.ageMax; a++) {
	  if(in(pop[X.Id].statuts[a],Statuts_FP))
		{pop[X.Id].statuts[a]=S_NONTIT;}
	  }
	}
	
} else
{
      // liquidation totale
      ageprimoliq        = int_mois(agetest, X.moisnaiss + 1 );
      ageliq             = int_mois(agetest, X.moisnaiss + 1 );
      agefin_primoliq    = agetest;
      agefin_totliq      = agetest;
      pliq               = pension;
      primoliq           = true;
      liq                = true;
      dar                = primoliq && dar  ;
}

   
   tp = 0;  // on initialise la dummy qui vaut 1 si départ au taux plein
// RQ : on définit l'indicatrice taux plein même si tous les droits ne sont pas liquidés

   //# détermination du taux plein (différent selon les régimes d'affiliation). 
   if ((pension+VFU_rg+VFU_ar+VFU_ag>0) && agefin_totliq)  // si pension non nulle et liquidation totale 
   {
      if (tauxliq_rg >= 1) tp=1;  // TP si taux liquidé au RG >=1
      else if(duree_fp_maj > l.DureeCibFP && tauxliq_fp > 1) tp=1; // TP si durée FP > cible FP et taux liq FP>=1
      else tp=0; // TP = 0 sinon
   }
   
   return;
```

### Droits dérivés: la méthode _SimDer()_

La méthode `SimDer()` consiste à simuler les droits dérivés de l'ensemble des individus dont le conjoint Y est décédé entre l'année présente _t_ et l'année antérieure _t-1_. `SimDer()` ne tient qu'à une ligne: une instance _liqrev_ de la structure *__Reversion__* y est créée. Cet objet qui figure dans les attributs de l'objet `X->retr` de l'individu contient une photographie de l'état des pensions de réversion lors de leur liquidation. Pour comprendre précisément ce en quoi consiste la fonction `simDer()`, nous encourageons à consulter la partie du [chapitre 2](chap2.md) consacrée à la présentation de la structure *__Reversion__*.

```cpp
// Méthode SimDer() de la structure Retraite  //

// 1 seule ligne: on crée liqrev, une instance de la structure Reversion 
// pour l'individu X, le conjoint Y, l'année t, l'année de législation anLeg
liqrev = make_shared<Reversion>(X,Y, t, options->anLeg);
```
