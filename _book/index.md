--- 
title: "destinieTutorial"
author: "Nagui Bechichi, Tom Olivia"
date: "2021-11-03"
site: bookdown::bookdown_site
documentclass: book
#bibliography: [book.bib, packages.bib]
# url: your book url like https://bookdown.org/yihui/bookdown
description: "destinieTutorial : Une documentation du modèle de microsimulation des retraites de l'Insee"
#biblio-style: apalike
#csl: chicago-fullnote-bibliography.csl
classoption: dvipsnames
link-citations: yes
subparagraph: true
#cover-image: "resources/logo-utilitr.png"
#favicon: resources/logo-utilitr.png
links-to-footnotes: true
---
# Objectifs du projet {-}



Ce chapitre introductif récapitule les objectifs de la documentation DestinieTutorial et les personnes pouvant trouver un intérêt à parcourir les différents chapitres.

## DestinieTutorial: Objectifs

### Un compromis entre documentation Doxygen et littérature sur Destinie 

Le projet DestinieTutorial a pour but de construire une documentation faisant office de compromis entre la documentation officielle du code source du modèle Destinie 2 et des différentes publications qui traitent de Destinie. Celle-ci doit se voir comme un complément à usage pratique du modèle.


[La documentation officielle du code source](https://github.com/InseeFr/Destinie-2/documentation) est produite automatiquement lors de la compilation du programme, grâce à Doxygen. Celle-ci est par nature tout à fait exhaustive, et permet d'étudier la structure de Destinie 2. Mais cette documentation est massive: 115 pages, où les classes (22) et les fichiers (23) sont détaillés par ordre alphabétique. L'étude de cette documentation représente un coût d'entrée important, en particulier si le lecteur n'a pas d'expérience de programmation en _C++_ ou ne maîtrise pas les principes généraux de la programmation orientée objet.


Plusieurs publications officielles détaillent le fonctionnement du modèle Destinie. Celles-ci sont absolument nécessaires pour comprendre les principes généraux de la microsimulation, et la structure de Destinie. Naturellement, le degré de détail de ces études concernant le code source du modèle est assez faible. 

Ainsi, DestinieTutorial pioche dans le code-source de Destinie 2 pour apporter une degré de détails supplémentaires nous semblant nécessaires à l'utilisation pratique du modèle.

### Fluidifier les passations de poste, et développer son usage

Le _turn-over_ fréquent au sein de l'équipe Destinie ainsi que chez nos partenaires traditionnels génère des risques de pertes de savoir-faire pouvant influer sur l'efficacité de nos travaux. Nous souhaitons que cette documentation alternative permette aux nouveaux arrivants de s'approprier plus aisément la logique du modèle Destinie 2. DestinieTutorial pourrait également permettre de fluidifier les travaux collaboratifs de l'équipe avec des partenaires  historiques  comme la Direction Générale du Trésor (DGT) ou le  Conseil d'Orientation des Retraites (COR).


Destinie 2 est un modèle respecté par la communauté des micro-simulateurs et chercheurs intéressés à la question du système de retraite français. Son code source, publié en ligne et programmé d'une manière très efficace, peut toutefois décourager des personnes souhaitant mobiliser pour la première fois un outil de micro-simulation. Nous espérons que cette documentation alternative permettra d'inciter davantage d'acteurs extérieurs à choisir Destinie pour réaliser leurs travaux d'étude.


### Organisation de la documentation DestinieTutorial

La documentation DestinieTutorial s'organise autour de chapitres au format .Rmarkdown divisés par thématique et hiérarchisés selon le déroulement des modules de Destinie. 

Le [chapitre 1](chap1.md) détaille les éléments qui sont nécessaires pour lancer une simulation à l'aide du modèle Destinie 2. 

Le [chapitre 2](chap2.md) présente l'organisation générale du code source de Destinie, ainsi que les structures principales qui sont fréquemment utilisées dans les fonctions. Les chapitres [3](chap3.md), [4](chap4.md) et [5](chap5.md) traitent respectivement des trois modules principaux de Destinie 2, à savoir le module démographie, le module marché du travail, et le module des départs à la retraite.

Les chapitres [6](chap6.html) et [7](chap7.html) donnent des exemples pratiques d'utilisation du modèle. Le [chapitre 6](chap6.html),suite directe du [chapitre 1](chap1.html), montre comment réaliser simplement une première simulation avec Destinie 2. Dans le [chapitre 7](chap7.md), nous montrons comment réaliser des variantes de simulation simples (ex: changement de cibles économiques) mais aussi plus complexes qui implique de modifier le code source du modèle (ex: intégrer une nouvelle législation et étudier son impact sur l'équilibre du système de retraite). Le format .Rmarkdown permet d'illustrer ces chapitres avec des blocs de code, des tableaux et des figures. Les deux derniers chapitres sont sauvés sous format .html afin de pouvoir y faire figurer des graphiques intéractifs, tandis que les chapitres 0 à 5 sont au format .md afin de les rendre directement lisibles depuis le dépôt Git de ce tutoriel


___

