



# Module démographie {#chap3}

Les trois chapitres suivants détaillent le déroulement des fonctions appelées dans *R* via la librairie Destinie. Pour une compréhension complète, nous invitons à parcourir en amont ou en parallèle le [chapitre 2](chap2.md) qui détaille les structures importantes utilisées par le modèle. Si des blocs de code commentés sont disponibles dans ce chapitre pour aider à la compréhension du modèle, nous invitons le lecteur à se reporter au dépôt _Git_ du projet s'il souhaite étudier l'intégralité du code source des fonctions. 


Ce chapitre porte sur le module démographie de Destinie. Celui-ci est réalisé à l'aide de la seule fonction `destinieDemographie(simul)`. Comme son nom l'indique, le module démographie consiste à projeter chaque année jusqu'à l'année de fin de simulation les évolutions démographiques appliquées à notre population initiale observée en 2009. Plus précisément, ce module réalise dans une boucle annuelle les simulations de décès, migrations, séparations, mises en couple et naissances. La fonction `destinieDemographie(simul)` est définie dans le fichier _Demographie.cpp_.
 

_____

## Une fonction découpée en cinq blocs successifs


Le module démographie est découpé en cinq fonctions majeures ayant des fichiers _.h/.cpp_ individuels (à l'exception des séparations et mises en couple qui sont dans un même fichier). Avant ces cinq étapes, la fonction se charge de convertir l'environnement simul vers les structures C++ correspondantes (voir [chapitre 2](chap2.md)). Après ces cinq étapes, les bases _ech_, _emp_ et _fam_ mises à jour sont réexportées dans l'environnement `simul` disponible dans _R_. La section suivante détaille le fonctionnement de ces sous-fonctions.

```cpp
// Fonction destinieDemographie() sous une forme simplifiée  //

// a) Importation l'environnement simul de R et conversion vers les objets C++ (voir chapitre 2)
  auto S = Simulation(env);

 // b) Coeur de la fonction, découpé en 5 sous-fonctions
	imputations_base();                  
  int max_sim=(options->AN_MAX%1900)+1;                                    // (étape 0) nettoyage
  for(int t : range(AN_BASE+1,max_sim)) 
  {  //boucle annuelle
	mortalite(t,options→mort_diff,true,options→sante,options→mort_diff_dip); // (étape 1) 
	//imputsante(t); (fonction optionnelle sur l'imputation de l'état de santé)
	migrant(t);                                                              // (étape 2)
	separation(t);                                                           // (étape 3)
	mise_en_couple(t);                                                       // (étape 4)
	naissance(t,options→tirage_simple_naiss);                                // (étape 5)
  }

// c) Sortie des résultats dans R: maj de ech, emp et fam:
// ->  exemple pour la table simul$ech:
  static auto df_ech = Rdout("ech",{"Id","sexe","anaiss","findet","ageMax","mere","pere",
                             "neFrance","emigrant","moisnaiss","taux_prim","typeFP","k",
                             "enf1","enf2","enf3","enf4","enf5","enf6","pseudo_conjoint",
                             "peudip","tresdip","dipl"}
                             ); // cette ligne recrée la table ech et la rend disponible dans R
  
  for(Indiv& X : pop) { // cette boucle remplit la table ech avec les nouvelles infos
    if(X.Id!=0)
    df_ech.push_line(X.Id,X.sexe,X.anaiss,X.findet,X.ageMax,X.mere,
    X.pere,X.neFrance,X.emigrant,X.moisnaiss,X.taux_prim,X.typeFP,
    X.k,X.enf[0],X.enf[1],X.enf[2],X.enf[3],X.enf[4],X.enf[5],
    X.pseudo_conjoint,X.peudip,X.tresdip,X.dipl
    );
  }
```



### étape 0: _imputations_base()_

Cette fonction est lancée en premier afin de remplir correctement l'ensemble des informations nécessaires à la poursuite des fonctions suivantes. Sans entrer dans le détail de la programmation de la fonction, voici les opérations qu'elle réalise:

- réordonnement des enfants (`X.enf`) du plus âgé au plus jeune;
- imputation des statuts d'activités (`X.statuts`) manquants et prolongation des carrières une fois la retraite passée;
- affinement des codes de statut (`X.statuts`): la fonction publique est déclinée selon actif/inactif, et fonction publique d'Etat / territoriale-hospitalière. L'invalidité est également distinguée selon le régime des individus (fonction publique, régime général, indépendant)
- création des identifiants de pseudo-conjoints (`X.pseudo_conjoint): on apparie les veufs avec des mariées et des veuvex avec des mariés selon leur âge de fin d'étude _findet_ et leur année de naissance _anaiss_. Cela permet de reconstruire les pensions de réversion lorsque l'on ne disposait pas d'information concernant la retraite du défunt conjoint.


Une fois ce nettoyage réalisé, les cinq sous-fonctions du bloc démographiques sont lancées successivement. L'ordre des opérations démographiques effectuées n'est pas dû au hasard. La mortalité intervient par exemple en premier afin de ne pas simuler au cours de la même année un évènement telle qu'une naissance ou une mise en couple à un individu qui décède. De même, les naissances dans un couple sont simulées une fois que les séparations et mises en couple ont été simulées.

### étape 1: _mortalite(t,options→mort_diff, option_mort_tirageCale=true,options→sante,options→mort_diff_dip)_;

La simulation des décès l'année _t_ consiste, pour chaque âge et chaque sexe, à attribuer une probabilité de décès aux individus présents en _t-1_ et à effectuer un tirage des personnes qui décèdent en _t_. Les arguments de la fonction mortalite permettent de choisir de considérer des probabilités de mortalité différentiées selon le diplôme/l'âge de fin d'études ou non, et le type de tirage des décès (calé ou non sur les cibles démographiques). Lorsque `option_mort_tirageCale` est spécifiée comme `True`, un tirage d’alignement par tri est retenu. Celui-ci retient comme cible de *K* décès le produit entre la probabilité de décès de l'individu et le nombre de présents en _t-1_.


1. On stocke l'identifiant des individus présents en _t-1_ dans un tableau *indiv_par_age* selon leur âge et leur sexe contenant les probabilités de survie des individus présents en _t-1_;

2. cible _p_ de mortalité pour chaque couple âge/sexe selon les valeurs renseignées dans l'objet `simul$Macro`, qui se trouve en _C++_ dans *M->q_mort*.

3. calcul d'une probabilité individuelle de décès selon le sexe, l'âge et l'année _t_. Lorsque  _option_mort_diff_ est active, cette probabilité dépend également de l'écart entre l'âge de fin d'études et l'âge moyen de fin d'études pour les individus du même sexe et de la même génération (4 modalités). Lorsque _options_mort_diff_dip_ est active, la strate additionnelle considérée dépend du diplôme obtenu (5 modalités). Sans ces options, la probabilité individuelle de décès est tout simplement _p_: elle dépend uniquement du sexe, de l'âge et de l'année _t_.

4. Tirage des décès: on crée la liste `ind_morts` des individus qui décèdent en _t_ selon l'option de tirage (cas classique: `option_mort_tirageCale = T`: c'est un tirage d'alignement par tri).

5. Pour les survivants, leur statut matrimonial en _t-1_ est prolongé et sera éventuellement mis à jour ultérieurement lors des fonctions *mise_en_couple(t)* et _separations(t)_. Le statut professionel est également prolongé provisoirement d'un an, mais pourra évoluer ultérieurement dans le module des transitions sur le marché du travail. Les conjoints de défunts deviennent veufs et pourront se remettre en couple dans la même année _t_.


### étape 2: _migrant(t)_;

La simulation des flux migratoires consiste chaque année à créer ou faire partir des individus par sexe et tranche d'âge selon le signe du solde migratoire de `simul$CiblesDemo` (dans _C++_, ces valeurs sont renseignées dans la structure **_ciblesDemo_**). Le solde est positif pour les enfants de 2 à 17 ans et les adultes de + de 27 ans: on crée donc des nouveaux individus correspondant à l'arrivée nette de migrants. Le solde est négatif pour les enfants de moins d'1 an et les jeunes de 18 à 26 ans: on fait donc partir des individus pour que le solde migratoire cible soit respecté.


1. (solde positif): Création des individus qui immigrent en France en _t_ (correspond au solde net positif de migrants): on augmente la population du nombre d'individus permettant d'être en cohérence avec les cibles démographiques. Les enfants immigrants créés en _t_ sont ensuite alloués aux mères immigrantes créées également en _t_ selon la minimisation d'une distance entre l'année de naissance de l'enfant et l'année de fin d'études de la potentielle mère.

```cpp
// Exemple de création d'entrants immigrés pour les hommes de 27+:  //
  for(int i : range(cible)) { 
    int age = age_arrive(HOMME,false,t); // on simule un age d'arrivée (min à 27 ans) 
    pop.emplace_back(pop.size(),HOMME,t+1900-age,18,age,0,0);   // incrément d'un nouvel individu en queue du vecteur pop
// IMPORTANT -> on fait appel au 2e constructeur de la structure Indiv, qui diffère de celui utilisé lors de la création du vecteur initial pop (dans Simulation.h)
//  Indiv(int Id, int sexe, int anaiss,  int findet, int age, int pere, int mere) -> initialise les attributs selon les valeurs spécifiées (voir chap2 et le fichier  Indiv.h pour + d'infos)
    
    Indiv& X = pop.back();  // X est l'indiv qui vient d'être créé
    X.findet = findet_migrant(X);  // On simule son âge de fin d'étude (findet)
    X.neFrance=0; // statut d'immigré
    X.statuts[age]=999;  // statut d'emploi initialisé
	  X.matri[age] = CELIB;  // statut matrimonial initialisé
	  X.moisnaiss=X.Id%12;  // mois de naissance aléatoire
	  X.dipl=(X.findet<16) ? SSDIPL : (X.findet==16) ? BREVET : (X.findet<18) ? CAP : (X.findet<20) ? BAC : UNIV;  // on détermine le diplôme selon l'age findet simulé
  }
  ```

2.	(solde négatif): Création des individus qui partent de France en _t_ (correspond au solde net négatif de migrants): on augmente la population du nombre d'individus permettant d'être en cohérence avec les cibles démographiques. On attribue à chaque individu sa probabilité d'émigrer selon son sexe, son âge et _t_. Cette probabilité est renseignée dans _ciblesDemo_. On crée la liste des individus (hommes et femmes) amenés à émigrer par un tirage selon les probabilités d'émigrer et la cible de départs (le solde migratoire net). On met à jours plusieurs attributs des individus qui émigrent: `ageMax=t-1`, `emigrant=1`. statut professionel et matrimonial réinitialisés.


NB: l'émigration des nouveaux nés est réalisée plus tard dans la fonction `naissance()`.

### étape 3: _separation(t)_;

La fonction `separation()` sélectionne les couples se séparant l'année _t_ et met à jour leurs attributs le cas échéant. Les probabilités de séparation considérées sont calculées en considérant des coefficients de régression estimés dans [Duée (2005)](https://www.epsilon.insee.fr/jspui/handle/1/5867).

1. Pour toutes les femmes mariées en _t-1_, calcule de la probabilité de séparation depuis un logit dont les coefficients sont donnés. Les variables explicatives considérées portent sur la durée passé en couple, l'âge, le nombre d'enfants, les enfants hors union, l'âge de fin d'études. Tirage non calé des femmes se séparant en _t_.

2. mise à jour des attributs des individus qui se séparent: on change le statut matrimonial de la femme et de son ancien conjoint.


### étape 4: _mise_en_couple(t)_;
La fonction `mise_en_couple()` sélectionne les individus célibataires qui vont se mettre en couple lors de l’année _t_. De la même façon que pour les séparations, les probabilité de mise en couple pour les hommes et femmes célibaraires sont estimées en considérant des coefficients de régression issus de [Duée (2005)](https://www.epsilon.insee.fr/jspui/handle/1/5867).

1. Calcul des probabilités de mise (ou remise) en couple en_t_, avec une distinction entre personnes jamais mariées d'un côté et veufs/séparés/concubins de l'autre.

2. Tirage sans cible de la liste des femmes qui se marient en _t_. Même procédure pour les hommes à marier en _t_.

3. On crée les couples en associant les hommes et les femmes à marier en _t_. Pour chaque femme à marier, un score est calculé pour chaque homme à marier selon l'écart d'âge et l'écart d'âge de fin d'études. On choisit l'homme avec le meilleur score.

4. Mise à jour des attributs (statut matrimonial, identifiant des conjoints)

```cpp
// Fraction du code de mise_en_couple(t) consistant à créer les couples //

 for (int e : indices(liste_F_amarier)) 
 {  // pour toutes les femmes à marier
    Indiv& X=pop[liste_F_amarier[e]]; // X est la femme à marier -> on va lui trouver un mari
    int indiceMari=0; // indiceMari est initialisé à 0 et sera rempli ultérieurement
    double score_tmp = 0;
    double score=999999;

      for (int i: indices(liste_H_amarier)) 
      {  // pour tous les hommes à marier Y
        Indiv& Y=pop[liste_H_amarier[i]];
        score_tmp=pow(Y.age(t)-X.age(t)- 0.25*min_max(Y.age(t)-20,0,20),2) +pow((Y.findet-X.findet),2);  // score calculé selon l'écart d'age ajusté (fct min_max on considère l'age femme - 0.25*1(si ageF<20,0,sinon si age>20, age-20 (pénalité bornée à +20) et l'écart d'age de fin d'études 
        if(score_tmp <= score) {
          score = score_tmp;  // on met à jour le score si score_tmp est inférieur au meilleur score actuel
          indiceMari=i;  // maj du mari (celui qui donne le score le plus faible)
        }
      }
	  int id_mari = (liste_H_amarier.size()!=0) ? liste_H_amarier[indiceMari] : 0 ; //id du mari qui a le score le plus faible -> qu'on souhaite apparier avec Y (0 si aucun candidat potentiel)
	  Indiv& MARI = pop[id_mari];  // individu homme qui est le mari
      if (abs(MARI.age(t)- X.age(t))<21) 
      { // si ce fameux mari a pas un écart d'age de + de 20 ans (sinon on ne match pas)
        X.matri[X.age(t)] = 2;  // m a j statut matrimonial de la femme
        MARI.matri[MARI.age(t)] = 2;  //  m a j statut matrimonial du mari
        MARI.conjoint[MARI.age(t)] = X.Id;  // m a j de l'id des conjoints
        X.conjoint[X.age(t)] = id_mari;
        nb_mari++;  // incrémente du nb de mariage qu'on a finalisé
		liste_H_amarier.erase(liste_H_amarier.begin() + indiceMari);  // on enleve le mari de la liste des H à marier (pour ne pas le matcher avec une autre femme dans la boucle)
      }
 }
```

### étape 5: _naissance(t,options→tirage_simple_naiss)_;

La fonction `naissance()` va sélectionner les mères mariées qui vont avoir un enfant dans l’année.

1. Boucle sur toutes les femmes mariées ayant arrêté les études et en âge de se reproduire (16-45 ans)

    - Attribution d'une probabilité individuelle de donner naisance selon plusieurs caractéristiques: nombre d'enfants, durée du couple, âge de fin d'études, indicatrice qui vaut 1 si le conjoint actuel est père du dernier enfant.
  
    - Sélection des mères par un tirage calé selon la cible de naissance `ciblesDemo->Naissances[t]`. Une fraction 105/205 (codé en dur dans le programme) de nouveaux-nés sont des garçons.
  
    - Création de l'individu correspondant au nouveau-né dans le vecteur pop, imputation de son âge de fin d'études et de son diplôme selon l'âge de fin d'études des parents (voir code détaillé plus bas).
    
    - Mise à jour des attributs de la mère et du père (nombre d'enfants et leurs identifiants respectifs).

  
  ```cpp
  // imputation de l'âge de fin d'études de l'enfant B (nouvel individu de pop) selon celui de la mère et du père:
 if (liste_nvellemere_garcon[i]) {  // si le nouveau-né est un garçon
  // imputation de l'âge de fin d'études
      findetnn=20.8-0.04825+0.22753*(nouv_pere.findet-20.8)+0.20332*(nouv_mere.findet-21.2)+2.68015*alea_adfe;
      liste_G.push_back(B.Id); // on incrémente le vecteur contenant l'ensemble des garçons nés en t
    }
    else {  // si le nouveau-né est une fille:
      findetnn=21.2+ 0.06294+ 0.2122*(nouv_pere.findet-20.8)+0.21972*(nouv_mere.findet-21.2)+2.61275*alea_adfe;
      liste_F.push_back(B.Id);
    }
    // maj des attributs du nouveau-né B:
    B.findet=min_max(findetnn,16,30)+alea();  // age de fin d'études déterminé selon findetnn
    B.neFrance=1;  // naissance en france
    B.statuts[0]=S_SCO;  // statut pro: à l'école
    B.ageMax=1;  // age max=1 (sera incrémenté les annés suivantes dans la boucle)
	  B.moisnaiss=B.Id%12;  // répartition homogène dans les 12 mois de l'année
  }
  
  ```
      
  
2.	Détermination d’enfants morts-nés, calage selon une cible basée sur le quotient de mortalité à l’année t (`M->q_mort[sexe](t,0)`)

3. Parmi les nouveaux-nés vivants, attribution du statut d’émigré à une partie, selon les cibles démographiques renseignées dans `ciblesDemo`. Pour les individus tirés, on change simplement l’attribut emigrant en 1, et on fixe ageMax à 0 (car départ à la naissance).

