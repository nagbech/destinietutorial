


# Structures principales du modèle {#chap2}

L'usage du modèle Destinie se fait depuis une librairie *R*. Pour réaliser une simulation, il suffit de lancer par étape dans *R* les 5 fonctions suivantes:`destinieDemographie(simul)`,`destinieTransMdt(simul)`,`destinieImputSal(simul)`,`destinieCalageSalaires(simul)`,`destinieSim(simul)`. Après chaque fonction, l'environnement _simul_ retourné dans _R_ aura été modifié. Si l'usage du modèle se fait intégralement depuis _R_, le code source de toutes ces fonctions est rédigé en _C++_ dans une logique orientée objet.


Ce chapitre détaille les structures importantes du modèle Destinie 2 et la logique générale d'architecture du modèle. Les chapitres [3](chap3.md), [4](chap4.md) et [5](chap5.md) expliqueront chronologiquement le fonctionnement des cinq fonctions appelées dans *R* pour réaliser des simulations avec Destinie. Pour un lecteur souhaitant réaliser directement les simulations, il peut se rendre au [chapitre 6](chap6.md), où un exemple de simulation est proposé. Pour les autres, ces chapitres permettent de prendre connaissance plus en profondeur du code source du modèle. Comme le veut la logique de ce tutoriel, l'intégralité du code ne sera pas détaillé ici. Si pour un lecteur certains points ne sont pas mentionnés de manière suffisamment exhaustive dans ce tutoriel, nous l'invitons à se reporter au dossier _src/_ contenant l'ensemble du code source du modèle. 

_____

## Un programme orienté objet


La logique de la construction du modèle Destinie 2 est celle de la Programmation Orientée Objet (POO). Un lecteur ayant déjà été introduit à la POO sur le langage *c++* ou un autre langage similaire comme *Java* aura davantage de facilité à comprendre la logique du code de Destinie 2. Nous tenterons toutefois de clarifier un maximum ce chapitre pour que les non-initiés à ce type de programmation puissent pleinement utiliser Destinie 2. Si des problèmes de compréhension persistent, nous conseillons au lecteur de parcourir en parallèle le MOOC gratuit de programmation en *C++* disponible sur OpenClassRooms.


Dans Destinie, les informations spécifiées dans l'environnement *simul* créé depuis un script *R* sont converties et réparties stratégiquement dans plusieurs objets *C++*. Ces objets sont des "structures" . Les structures, de la même manière que les classes, sont des objets ayant deux grandes propriétés. D'une part, elles permettent de stocker en tant qu'attributs une liste d'information de différentes nature (des vecteurs, de tableaux, des chaînes de caractère ou encore d'autres structures). De l'autre, elles peuvent intégrer des méthodes, qui sont des fonctions spécifiques consistant à modifier la valeur d'un ou plusieurs de ses attributs. Toute structure possède au moins une méthode de type "constructeur", qui comme son nom l'indique consiste à créer et initialiser une instance de la structure, c'est-à-dire un objet ayant les propriétés de la structure. Par convention, on écrit une majuscule à la première lettre d'une structure, et on écrit l'instance d'une structure en minuscule. `vector<Indiv>: pop` est par exemple un vecteur appelé _pop_ et constitué d'instances de la structure Indiv.


La figure ci-dessous est un schéma de la logique de programmation du code de Destinie. Chaque table représente une structure importante du modèle. Le carreau du haut liste les attributs principaux, et le carreau du bas détaille les méthodes principales. Les flèches donnent les liens qui existent entre les structures: en effet, une structure peut avoir parmi ses attributs une instance d'une autre structure. Par exemple, la structure **_Indiv_** qui stocke les informations pour chaque individu possède un attribut appelé _liqrev_ qui est une instance de la structure **_Reversion_**: c'est cette structure qui contient les méthodes permettant de calculer et mettre à jour les pensions de réversion. 
<div class="figure">
<img src="destinieClasses.png" alt="&lt;b&gt;&lt;i&gt;Figure 1 : Structures principales du code source de Destinie 2&lt;/i&gt;&lt;/b&gt;" width="100%" />
<p class="caption">(\#fig:unnamed-chunk-1)<b><i>Figure 1 : Structures principales du code source de Destinie 2</i></b></p>
</div>


Dans le dossier du code source _src/_, chaque structure majeure est spécifiée dans des fichiers au format _.h_  et _.cpp_ du même nom. Les fichiers _h_ sont des fichiers d'en-tête: ils consistent simplement à définir la structure et l'ensemble de ses éléments (attributs et méthodes). Les fichiers _.cpp_ sont utilisés pour détailler le contenu des méthodes de la structure.

Dans les parties qui suivent, nous détaillons chaque structure pour comprendre leur fonction et leur lien avec les données inscrites dans l'environnement *simul* créé dans le [chapitre 1](chap1.md).

_____

### Simulation: une structure pour convertir les données *R*
La structure **_Simulation_**  a la tâche de convertir de R vers C++ l’intégralité des informations utiles à la réalisation des simulations. **_Simulation_** est une structure fondamentale dans Destinie. En effet, les 5 fonctions majeures de Destinie appelées via *R* vont toutes commencer par construire une instance de **_Simulation_** pour stocker dans *C++* l'ensemble des informations nécessaires à la réalisation de chacune des fonctions. Cette structure est définie dans les fichiers _Simulation.cpp/.h_.

#### Attributs principaux

- `vector<Indiv>: pop`  -> Les tableaux contenant les informations sur les individus (`simul$ech`,`simul$emp` et `simul$fam`) sont stockées dans un vecteur _pop_ contenant autant d'instances de la structure _Indiv_ qu'il y a d'individus. Lors de la simulation des trajectoires démographiques, certains individus sont créés (naissance, migration). En conséquence, le vecteur _pop_ s'aggrandit pour contenir ces nouvelles informations. Nous détaillons plus bas le contenu de la structure _Indiv_.

- `ptr<Macro>: M` ->   L'instance *M*^[En réalité, *M* est un pointeur vers une instance de **_Macro_**. Comme cela n'a pas d'importance fondamentale pour la compréhension de la logique de programmation du code de Destinie 2, nous choisissons dans tous les chapitres de ne pas traiter de la question de la différence conceptuelle entre un objet et un pointeur.] de la structure **_Macro_** est la conversion pure de `simul$macro`, et comporte autant d'attributs que de colonnes dans `simul$macro`. Les tables de mortalité  (quotients de mortalité, survie, espérance de vie), localisées en dehors de `simul$macro`, sont également imbriquées à la structure **_Macro_** dans les attributs `q_mort`,`survie` et `espvie`.

- `ptr<Options>: options` -> Les options de simulation spécifiées par l'utilisateur dans `simul$options` sont stockées dans une instance *options* de la structure **_Options_**.  Celle-ci consiste simplement à stocker tous les éléments spécifiées dans `simul$options` en autant d'attributs. Notons également que la structure **_Options_** présente potentiellement plus d'attributs que les valeurs inscrites dans `simul$options`: quand l'utilisateur ne spécifie pas de valeur pour une option, une valeur par défaut est ainsi attribuée à l'objet *options* dans l'attribut correspondant.

- `ptr<CiblesDemo>: ciblesDemo` -> Les valeurs renseignées dans *R* dans `simul$ciblesDemo` sont stockées dans une instance *ciblesDemo* de la structure **_CiblesDemo_**. Ici également, la structure est simplement un ensemble d'attributs correspondant à chaque colonne de `simul$ciblesDemo`.

- `vector<EqSante> eq_sante` -> Les valeurs de `simul$eq_sante` sont stockées selon la même logique dans la structure **_EqSante_**. Ce sont les coefficients de régression des transitions d'état de santé.

Note: la définition des structures créées pour ces quatre attributs est définie dans *Simulation.h*. D'autres attributs sont compris dans la structure Simulation, nous invitons les lecteurs à consulter le code source du modèle pour avoir une vision intégrale de chaque structure.

#### Méthode principale: constructeur

Une instance de **_Simulation_** est créée au début de chaque fonction de Destinie. La création d'une structure se fait selon une méthode appelée le constructeur. C'est à l'intérieur de ce constructeur que les informations stockées sur *R* dans l'environnement *simul* sont alloués aux bons attributs. L’attribut *pop* est le seul à nécessiter un travail important de construction, car les informations sont disséminées dans `simul$ech`,`simul$emp` et `simul$fam`. Les autres attributs ne sont que des copies des objets éponymes inclus dans *simul*.

**Création de pop:** une boucle sur tous les individus de `simul$ech` est réalisée: à chaque incrément,un objet Indiv s’ajoute au vecteur pop à l’aide du constructeur de la structure Indiv qui sera détaillée plus bas dans ce document.

```cpp
pop.clear();
  pop.reserve(200000);
  pop.emplace_back(); // ajoute l'individu fictif d'identifant 0
  int j=0, k=0;
  //on remplit pop en créant autant d'instances d'Indiv que d'individus présents dans simul$ech
  for(int i : range(ech.Id.size())) { 
      pop.emplace_back(ech,emp,fam,i,j,k); // constructeur d'Indiv: prend en argument ech, emp et fam de simul    
  }
  for(auto& X : pop) {  // création pour chaque indiv d'un attribut retr, instance de Retraite (utile plus tard)
    X.retr = make_shared<Retraite>(X,50);
  }

```

C'est également dans **_Simulation_** que sont définies les méthodes qui permettent l'export de deux tables de sortie du modèle, à savoir `simul$retraites` et `simul$liquidations`

_____

### Indiv: une structure pour stocker les informations des individus

La structure **_Indiv_** consiste à organiser les données stockées dans `simul$ech`,`simul$emp` et `simul$fam`. Cette structure est définie dans les fichiers _Indiv.cpp/.h_.

#### Attributs principaux

Parmi les attributs d'Indiv, il y a des caractéristiques individuelles provenant de `simul$ech` et `simul$emp`. Celles-ci peuvent être unidimensionelles (comme *Id*,*sexe*,*anaiss*) ou des vecteurs par âge (comme *salaires* ou *statuts*). Les caractéristiques familiales issues de `simul$fam` peuvent aussi être des vecteurs par âge (comme *enf* et *anaissEnf*) ou des valeurs uniques (*pere*, *mere* qui stocke l'identifiant des parents). 

Un autre attribut important est *retr*: qui stocke une instance de la structure  **_Retraite_**, afin d'associer à l'individu la valeur détaillée de sa pension. Cet attribut est mis à jour dans la dernière fonction `destinieSim()` qui simule la liquidation. 

#### Méthodes principales: constructeurs
Deux constructeurs sont définis pour créer une instance de la structure **_Indiv_**. 

1. `Indiv(Ech& ech, Emp& emp, Fam& fam, int& i, int& i_e, int& i_f)`: Les attributs tirés de _ech_ vont être simplement copiés, tandis que les caractéristiques sous forme de vecteur (attributs professionnels et familiaux tirées de _emp_ et de _fam_) sont copiés à l’aide d’une boucle qui copie l’ensemble des informations pour l’individu. 


```cpp
// Exemple du remplissage de statuts et salaires:
int j_size = emp.Id.size();
 while(i_e < j_size && emp.Id[i_e] == Id) {
    int age = emp.age[i_e];
    statuts[age] = emp.statut[i_e];
    salaires[age] = emp.salaire[i_e];
    if(statuts[age] != 0) ageMax = age+1;
    i_e++;
  }
```

2. `Indiv(int Id, int sexe, int anaiss,  int findet, int age, int pere, int mere)`: Ce second constructeur est utilisé dans le code lorsque des individus sont ajoutés à la liste après une naissance ou des migrations par exemple. Naturellement, l'instance **_Indiv_** n'est dans ce cas pas construite à l'aide d'informations de _ech_, _emp_ et _fam_.

_____

### Retraites: une structure contenant toutes les informations sur la retraite des individus

A la différence de la structure **_Indiv_**, **_Retraites_**, définie dans les fichiers _Retraites.cpp./.h_ ne s'appuie pas sur les informations envoyées par l'utilisateur dans _R_ via l'environnement _simul_. Les instances de la structure **_Retraites_** sont créées une fois que le modèle simule les droits à la retraite des individus.


**_Retraites_** va stocker pour chaque individu à chaque âge la pension de retraite perçue à un niveau détaillé. C'est dans ses méthodes que l'on retrouve les fonctions permettant de calculer ces montants. Chaque individu possède dans ses attributs une instance de la structure **_Retraites_** appelée _retr_. _X→retr_ est vide, jusqu’à ce que l’on simule effectivement la liquidation de tous les individus dans les étapes finales de Destinie (principalement via la fonction `destinieSim()`, dernière fonction lancée depuis _R_). Ainsi, une fois que la simulation des départs à la retraite a été réalisée, on accède aux informations de retraite de chaque instance de **_Indiv_**, i.e chaque individu, par son attribut _retr_.


_retr_, comme toute instance de **_Retraites_** va notamment compter dans ses attributs des instances de deux autres sous-structures détaillées ultérieurement: **_DroitsRetr_**  qui a pour but de calculer les droits à la retraite à la liquidation selon l’historique sur le marché du travail, l’age et la législation en vigueur et **_Reversion_** qui calcule le montant des droits dérivés.


#### Attributs principaux

- `Indiv X; int age`: c'est l'individu qui est lié à l'instance de la structure Retraite (i.e l'individu pour lequel on va calculer les pensions). On mentionne également son âge car les retraites sont calculées à chaque âge.

- `double pension_i = 0; double rev_i=0` (avec i: _tot_, _rg_, _fp_, _in_, _ar_, _ag_, _ag_ar_): informations concernant le montant des pensions et des droits dérivés (de base et complémentaires) pour l’individu _X_ à l’age _age_ pour chaque régime de retraite


- `double min_vieil = 0`: montant perçu pour l'ASPA (minimum vieillesse). Cet attribut n'est renseigné que pour la personne de référence du ménage. En cas de perception de la prestation, c'est le différentiel entre le montant de l'ASPA et la pension de retraite.

- `ptr<DroitsRetr> primoliq, totliq, liq`: instances de la structure **_DroitsRetr_**. En effet comme l'instance de **_Retraites_** évolue tous les ans pour les individus afin notamment de revaloriser les pensions, ces attributs permettent se photographier durablement l'état des pensions lors de la liquidation de l'individu.


- `ptr<Reversion> liqrev`: instance de la structure **_Reversion_** pour photographier durablement l'état des pensions de réversion lors de leur liquidation.


Pour résumer, pour chaque individu de notre population, i.e pour chaque instance de **_Indiv_** _X_ du vecteur _pop_: 

-	_X→retr_ est une instance de **_Retraites_**: contient de façon détaillée la pension perçue par _X_ à l’âge courant.

- *X→retr→primoliq,totliq,liq* sont plusieurs instances de **_DroitsRetr_**: contiennent une photographie de la situation de l’individu au moment de la (ou des) liquidation(s).

- *X→retr→liqrev* est une instance de **_Reversion_**: contiennent une photographie de la situation de l’individu au moment de la liquidation de ses droits dérivés.

#### Méthodes principales

A la différences des structures détaillées précédemment, **_Retraites_** contient plusieurs méthodes qui sont particulièrement importantes pour le fonctionnement de Destinie, et en particulier du module de simulation des départs à la retraite. 

##### Constructeur
L'instance de **_Retraites_** est créée en remplissant les attributs _X_ (l'individu concerné) et _age_ (son âge). Dès la construction, deux méthodes sont directements lancées `revaloDir()` et `revaloDer()`. Ces méthodes sont décrites plus bas. 
```cpp
// Constructeur de la structure Retraite
  Retraite::Retraite(Indiv& X, int t) : X(X),   age(X.age(t))                         
  {
    revaloDir(t);
    revaloDer(t);
  }
```
En pratique, l'instance est construite une seule fois dans **_Simulation_** lorsque l'on crée le vecteur _pop_ des individus, et l’année t est initialisée à 50 (année de début de simulation): `X.retr = make_shared<Retraite>(X,50);`.

##### Autres méthodes importantes

- `void SimDir(int age)`: teste la liquidation pour l'individu _X_ à l'âge _age_ et calcule le cas échéants les droits directs. Pour ce faire, `SimDir()` appelle des méthodes de la structure **_DroitsRetr_** qui rempliront les pointeurs _primoliq_, _totliq_ et _liq_. Un focus sur la méthode `SimDir()` est fait dans le [chapitre 5](chap5.md) qui porte sur la fonction `destinieSim()`, dernière fonction appelée dans _R_.


- `void SimDer(Indiv &Y, int t) `: initialise ou projette des droits dérivés. Cette méthode remplit l'attribut _liqrev_ (instance de **_Reversion_**) afin de rapatrier la pension de réversion de l'individu à l'année _t_. _Y_ est ici l'identifiant de l'individu ayant até marié avec l'individu _X_. 


- `void revaloDir(int t)` : revalorisation des pensions de droits directs à l'année _t_. Pour ce faire, la méthode teste si la liquidation a eu lieu à l’année _t_ en regardant si _primoliq_, _totliq_, _liq_ ont été remplis, et met à jour les attributs décomposés de pension le cas échéant en revalorisant les pensions par _revaloFP_ ou _revaloRG_ (attributs de _M_ renseignés initialement depuis _R_ dans `simul$Macro`) et en appliquant différentes règles légales.

- `void revaloDer(int t)` : revalorisation des pensions de droits dérivés à l'année _t_. Pour ce faire, la méthode teste si la liquidation des droits dérivés a eu lieu à l’année _t_ en regardant si _liqrev_ a été rempli, et met à jour les attributs décomposés de pension de réversion le cas échéant en revalorisant les pensions par _revaloFP_ ou _revaloRG_ (attributs de _M_ renseignés initialement depuis _R_ dans `simul$Macro`) et en appliquant différentes règles légales.


- `void minvieil(int t)` : calcule le montant de l'ASPA auquel a droit le cas échéant le ménage à l'année _t_.



_____

### Les structures associées à Retraites: DroitsRetr/Leg et Reversion

La structure **_Retraites_** s'appuie sur deux autres structures pour calculer les pensions de droits directs et dérivés. Ces structures, **_DroitsRetr_** et **_Reversion_**, sont définies respectivement dans _DroitsRetr.h/cpp_ et _Reversion.h/cpp_. C'est dans une instance de **_DroitsRetr_** que sont calculés les droits à la retraite pour chaque individu et chaque année, et selon une certaine législation. C'est dans une instance de **_Reversion_** que l'on calcule la liquidation de réversion.


#### DroitsRetr

Cette structure stocke les éléments constitutifs des droits directs (montant à la liquidation, majorations, durée validée, coefficient de décote/surcote et calcule les pensions de droits directs des régimes de base. Un objet de cette structure est créé pour chaque individu à chaque âge testé. Un objet (ou deux si la liquidation a lieu en deux étapes) est ensuite conservé durablement uniquement en cas de liquidation dans l'attribut correspondant (_primoliq_, _totliq_ ou _liq_).

##### Attributs: caractéristiques de la liquidation

- `Indiv& X` : c'est l'individu qui est lié à l'instance de **_Retraites_**
- `Leg& l` : c'est une instance de la structure **_Legislation_**: c'est ici que sont stockées les règles légales de départs à la retraite pour l'année considérée.
- `double agetest`  : c’est l’age (à la fin du mois) de l'individu lorsque l'on effectue le test de liquidation
- Attributs correspondants à plusieurs caractéristiques de la pension (initialisés à 0): âge(s) de liquidation, durées validées, taux de liquidation, majoration des pensions, salaire de référence et salaire annuel moyen, cumul des points pour les régimes complémentaires, montant du versement forfaitaire unique (VFU), pension à liquidation dans les différents régimes, taux de proratisation, (...)

##### Méthodes principales:

- Constructeur: à la construction, l’instance de **_DroitsRetr_** pour l’individu _X_, la législation _leg_ à l’age _agetest_ est créée en remplissant les attributs _X_, _l_ et _agetest_. Les autres attributs sont fixés initialement à 0.
```cpp
DroitsRetr::DroitsRetr(const Indiv& X, Leg& leg, double agetest) :
    X(X), l(leg), agetest(agetest) // on remplit X, l et agetest
{
    datetest = arr_mois(X.anaiss + agetest, X.moisnaiss + 1 );  // date lors du test
    age     = int_mois(agetest, X.moisnaiss + 1 ); // âge atteint l'année de l'agetest
    t = X.date(age); // année
}  
```
- Des méthodes pour mettre à jour les attributs de durée: 

  - `double duree_trim(const vector<int>& statuts_cpt, int anneLimite= options→AN_MAX)` :  calcule la durée en trimestres passée dans un ou plusieurs statuts
  - `void durees_base()` : calcule les durées cotisées dans les différents régimes et la durée AVPF
  - `void durees_majo()` : cette méthode repart des durées calculées par durees_base(), et calcule les durées majorées
  - `void DecoteSurcote()` : calcule les durées de décote et surcote et les taux de liquidation

- Des méthodes pour savoir si l'individu a ses droits ouverts:

  - `int AgeMin()` :  retourne 1 si l'individu a atteint l'âge minimal d'ouverture des droits
  - `double AgeMax()` : retourne l'âge de mise à la retraite d'office de l'individu 
  
- Des méthodes pour le calcul des pensions à liquidation 

  - `void LiqPrive(); void LiqPublic()` : calcul de la pension privée et publique 
  - `void Liq()` : simule la liquidation à l'âge courant. Modifie les attributs _pension_rg_, _pension_fp_ et _ageliq_
  - `void SecondLiq()` : calcule les pensions hors FP en cas de seconde liquidation

##### La structure Leg
Une instance de la structure **_DroitsRetr_** contient dans ses attributs une instance d'une autre structure appelée **_Leg_** (écrite _Législation_ dans la figure 1). Cette structure, définie dans  _Legislation.h/cpp_, a pour but de définir l'ensemble des paramètres légaux en vigueur pour un couple (individu, date). Cet objet permet de vérifier si l'individu peut liquider ses droits compte tenu de la législation en vigueur à la date du test.


Ses attributs sont les règles légales en vigueur:

- l'année de législation (_an\_leg_, _an\_leg\_DRA_, _an\_leg_, _an\_leg\_SAM_, _LegMin_, _LegDRA_, _LegSAM_)

- Règles paramétriques portant sur plusieurs dimensions:
  - l'âge (_AgeAnnDecFP_,  _AgeMinMG_, _AgeMaxFP_, etc.)
  - la durée (_DureeMinFP_, _DureeProratFP_, _DureeCibRG_, etc.) 
  - les décotes/surcotes
  - d'autres critères réglementaires (_DecoteFP_, _DecoteRG_, _SurcoteRG_, etc.) ;

Méthodes principales:

- Constructeur `Leg(const Indiv& X, int age, int an_leg)`: va mettre à jour tous les attributs selon l'individu, son âge et l'année de législation. Des méthodes intermédiaires y sont appelées. Les trois principales étant inscrites en dessous.

- Mise à jour des attributs selon la nature de la pension:
  - publique: `leg_priv(X,age)`
  - privée: `leg_priv(X,age)`
  - DRA (retraite anticipée): `leg_dra(X,age)`
  
#### Reversion

Cette structure stocke les éléments constitutifs des droits dérivés (montant de la réversion pour chaque régime).

##### Attributs: caractéristiques des droits dérivés

- `double rev` : montant total de la réversion à la liquidation
- `double re_i` : pour chaque régime (RG, GP, RSI, AGIRC, ARRCO, AG-AR), montant de la réversion à liquidation
- `int ageliq_rev` : âge à la liquidation de la pension de réversion
- `int nbEnfCharge` : nombre d'enfants à charge lors du décès du conjoint
- `int ageliq_rev`  : indentifiant du défunt


##### Méthode principale: constructeur

La seule méthode de la structure **_Reversion_** est son constructeur. Développé dans une logique assez différentes d'autres structures de Destinie, c'est dans le constructeur que l'ensemble des opérations permettant d'obtenir les caractéristiques de la reversion est réalisé.

A la construction, l’instance de **_Reversion_** pour l’individu _X_, lors du décès de son conjoint _Y_ à la date est initialisée de la manière suivante:
```cpp
// Constructeur d'une instance de la structure Reversion //

Reversion::Reversion(Indiv & X, Indiv & Y, int t, int legRetroMax) :
      rev(0), rev_rg(0), rev_fp(0), rev_in(0), rev_ag(0),   // initialisation des attributs à 0
      rev_ar(0), rev_ag_ar(0), ageliq_rev(0), nbEnfCharge(0),idConj(Y.Id) // seul attribut !=0: id du conjoint Y
{
    nbEnfCharge = X.NbEnfC(t); // calcul du nb d'enfants à charge
    int age_y = 1900 + t - Y.anaiss;  // age de X et Y
    int age_x = 1900 + t - X.anaiss;
  
    double agetest_y = arr_mois(t-Y.anaiss%1900, -Y.moisnaiss); // age du test de liquidation du conjoint Y
    
    Leg l_y = Leg(Y, age_y, min(Y.anaiss+age_y,legRetroMax));  // legislation appliquée pour la retraite de Y
    Retraite retraite_y(Y,t); // création d'un objet retraite relatif à la pension du conjoint Y à la date t
    
    
    // copie des attributs de liquidation de Y.retr vers retraite_y
	retraite_y.primoliq = Y.retr->primoliq;
	retraite_y.totliq = Y.retr->totliq;
	retraite_y.liq = Y.retr->liq;
	retraite_y.revaloDir(t);
	
	// Si le conjoint décédé n'avait pas tout liquidé -> calcul d'une pension théorique
  
	ptr<DroitsRetr> dr = make_shared<DroitsRetr>(Y,l_y,agetest_y); droits à la retraite pour Y à l'age du test
	
	if( !retraite_y.totliq) // si liquidation totale non réalisée au moment du décès
	{ 
     
     dr->l.AgeMinRG = 0; // réinitialisation de l'age min RG/FP
     dr->l.AgeMinFP = 0;
     
     dr->durees_base();  // calcul des durées cotisées
     dr->durees_majo();
     dr->DecoteSurcote();
     
     if( !retraite_y.primoliq ) { // si Y n'as pas primoliquidé au moment du décès
        dr->tauxliq_rg=1; // on fixe les taux de liquidation à 1 (TP)
        dr->tauxliq_ar=1;
        dr->tauxliq_fp=1;
        dr->Liq();  // on applique la liquidation
     }
     else if( !retraite_y.totliq ){ // si Y a primoliquidé et n'a pas liquidé totalement lors du décès
        dr->tauxliq_rg=1; // on fixe les taux de liquidation à 1 (TP)
        dr->tauxliq_ar=1;
        dr->SecondLiq(); // on applique la 2e liquidation
     }
     retraite_y.liq = dr; // on associe aux attributs liq,totliq,primoliq de retraite_y les droits ainsi calculés
     retraite_y.totliq = dr;
     retraite_y.primoliq = dr;    
  }
  
  
  retraite_y.revaloDir(t); // revalorisation des droits directs de Y
  
  double duree_rg_maj = retraite_y.liq->duree_rg_maj;
  double duree_in_maj = retraite_y.liq->duree_in_maj;
  
  // On re-simule ensuite la réversion
  // Reversions sans conditions de ressources: tx de reversion * retraite du conjoint Y

  rev_ar = M->TauxRevARRCO[t] * retraite_y.pension_ar; // Agirc
  rev_ag = M->TauxRevAGIRC[t] * retraite_y.pension_ag; // Arrco
  rev_fp = M->TauxRevFP[t]    * retraite_y.pension_fp; // FP
  rev_ag_ar = M->TauxRevARRCO[t] * retraite_y.pension_ag_ar; // Agirc-Arrco
  rev_in= M->TauxRevInd[t]  * (retraite_y.pension_in-retraite_y.liq->majo_3enf_in); // indépendants
  
  
  // reversion du régime général
  double partmin;
  double rev_lura;
  //double ratio_rg=0;
  Leg l_x = Leg(X, age_x, min(X.anaiss+age_x,legRetroMax)); // legislation en vigueur pour X à la date du test
  
  if (t<104 || l_x.an_leg<2004) // avant 2004
  {
     partmin  = min_max(duree_rg_maj / max(15.0, duree_rg_maj), 0, 1);
     rev_rg = M->TauxRevRG[t] *(retraite_y.pension_rg-retraite_y.liq->majo_3enf_rg);
     // on écrète la réversion du RG entre un min et un max:
     rev_rg = min_max(rev_rg, (M->MinRevRG[t] * partmin), M->MaxRevRG[t]);
     // rq: partmin s'applique uniquement à MinRevRG
  }
  else if (options->SAMRgInUnique ||
  (dr->l.LegSAM >= 2013 && 
  (t >= 117  && retraite_y.liq->t>=117) && 
  !options->SAMSepare))
  {// réversion (et droits directs associés) liquidés après 2017 
   //=> on est donc sous LURA (régime alignés entre RG et indépendants)
	 partmin = min_max((duree_rg_maj+duree_in_maj) / max(15.0, duree_rg_maj + duree_in_maj), 0, 1);
	 //if (duree_rg_maj+duree_in_maj>0) ratio_rg = duree_rg_maj / (duree_rg_maj+duree_in_maj);
	 double ratio_rg = (duree_rg_maj+duree_in_maj) >0 ? duree_rg_maj / (duree_rg_maj+duree_in_maj) : 0;
	 double ratio_in = (duree_rg_maj+duree_in_maj) >0 ? duree_in_maj / (duree_rg_maj+duree_in_maj) : 0;
     rev_lura =M->TauxRevRG[t] * (retraite_y.pension_rg-retraite_y.liq->majo_3enf_rg+retraite_y.pension_in-retraite_y.liq->majo_3enf_in);
     rev_lura =min_max(rev_lura, (M->MinRevRG[t] * partmin), M->MaxRevRG[t]);
	 rev_rg=rev_lura*ratio_rg;
	 rev_in=rev_lura*ratio_in;
  }
  else
  {  // entre 2004 et 2017: durée cotisée en tant qu'indépendant compte dans le quotient de partmin
     partmin = min_max(duree_rg_maj / max(15.0, duree_rg_maj + duree_in_maj), 0, 1);
     rev_rg =M->TauxRevRG[t] * (retraite_y.pension_rg-retraite_y.liq->majo_3enf_rg);
     rev_rg =min_max(rev_rg, (M->MinRevRG[t] * partmin), M->MaxRevRG[t]);
  }

  // majoration de la pension de réversion en fonction du nombre d'enfants
  double tauxmajo_rg=0;
  double tauxmajo_in=0;
  if(X.nb_enf(X.age(t)) >= 3) { tauxmajo_rg = 0.1; tauxmajo_in = 0.1;} // si 3 enfants ou +
  rev_rg*=(1+tauxmajo_rg);
  rev_in*=(1+tauxmajo_in);
  
  // Calcul de la réversion globale
  rev = rev_rg + rev_ar + rev_ag + rev_fp + rev_in;
  
  ageliq_rev = age_x; // maj de l'âge de liquidation de la réversion pour X
  
}  
```
  
